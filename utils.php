<?php
function build_organization_select($program_id, $current, $include_other, $only_late,
                                   $disabled=false, $name="o", $id=null)
{
    global $db, $lang;

    $sql = "SELECT * FROM {$db->prefix}organizations " .
               "WHERE program_id = ? " .
           "ORDER by title";
    $list_data = $db->query($sql, $program_id);

    $option_name = $lang->get('select_organization');
    $n = ($name !== null) ? " name='{$name}'" : '';
    $i = ($id !== null) ? " id='{$id}'" : '';
    $d = $disabled ? " disabled" : '';

    $organization_select = "<select$n$i$d>";

    $selected = $current == 0 ? " selected" : "";
    $organization_select .= "<option value='0'$selected>{$option_name}</option>";

    foreach ($list_data as $row)
      {
        $organization_title = htmlspecialchars($row['title']);
        $id = $row['id'];
        $selected = $current == $id ? " selected" : "";
        $d = ($only_late && $row['late_submission'] == 0) ? " disabled" : "";
        $organization_select .= "<option value='$id'$selected$d>{$organization_title}</option>";
      }

    $option_name = $lang->get('other');
    if ($include_other) {
        $selected = $current == -1 ? " selected" : "";
        $organization_select .= "<option value='-1'$selected>{$option_name}</option>";
    }
    $organization_select .= "</select>";

    return $organization_select;
}

$VALID_OPINIONS = array('n', 'y', 'c', 't', 'x', 'g', 'w', 'f');
# Opinions that indicate an interest in accepting a project
$ACCEPT_OPINIONS = array('g', 'w', 'f');

function build_opinion_select($program_data, $current, $name, $disabled=false) {
    global $VALID_OPINIONS, $lang;

    $d = $disabled ? ' disabled' : '';

    $result = "<select name='{$name}'$d>";

    $concurrent_with_gsoc = gmdate("m", $program_data['dl_student']) <= 6;

    foreach ($VALID_OPINIONS as $opinion) {
        # If the program doesn't select in the spring, accepting for GSoC
        # isn't an option
        if (!$concurrent_with_gsoc && $opinion != $current && $opinion == 'g')
            continue;

        $selected = $opinion == $current ? ' selected' : '';
        $name = $lang->get("opinion_" . $opinion);
        $result .= "<option value='{$opinion}'{$selected}>{$name}</option>";
    }

    $result .= "</selected>";

    return $result;
}

function opinion_is_valid($value) {
  global $VALID_OPINIONS;

  return in_array($value, $VALID_OPINIONS);
}

function opinion_is_accept($value) {
  global $ACCEPT_OPINIONS;

  return in_array($value, $ACCEPT_OPINIONS);
}

function build_project_badges($role, $project_data, $submit_count, $accept_count) {
    global $lang, $user;

    $result = '';
    if ($project_data['is_withdrawn'] == 1) {
        $result .= '&nbsp;<span class="badge">' . $lang->get('withdrawn') . '</span>';
    } else if ($role == 'm' || $user->is_admin) {
        if ($accept_count > 1 && opinion_is_accept($project_data['org_opinion'])) {
            $result .= '&nbsp;<span class="badge badge-important" title="' .
                $lang->get('multiple_projects_accept') . '">' .
                $lang->get('multiple_projects') . '</span>';
        } else if ($submit_count > 1) {
            $result .= '&nbsp;<span class="badge badge-info" title="' .
                $lang->get('multiple_projects_submit') . '">' .
                $lang->get('multiple_projects') . '</span>';
        }
    }

    if ($role == 'm' || $user->is_admin) {
        if ($project_data['is_accepted'] == 1) {
            $result .= '&nbsp;<span class="badge badge-success">' . $lang->get('accepted') . '</span>';
        }
        if ($project_data['is_accepted'] == 0) {
            $result .= '&nbsp;<span class="badge badge-warning">' . $lang->get('rejected') . '</span>';
        }
    }

    return $result;
}

class ProjectPermissions {

    public $can_apply_student;
    public $can_apply_mentor;
    public $can_submit;
    public $late_submission;
    public $is_owner;
    public $can_view;
    public $can_edit;
    public $can_change_organization;

    private function any_late_submissions($program_data) {
        global $db;

        $sql = "SELECT COUNT(*) AS count from {$db->prefix}organizations " .
               "WHERE program_id = ? AND late_submission = 1";
        $row = $db->query($sql, $program_data['id'], true);
        return $row['count'] > 0;
    }

    function __construct($program_data, $role, $project_data) {
        global $cache, $core, $db, $user;

        $can_apply_student = false;
        $can_apply_mentor = false;
        $can_submit = false;
        $late_submission = false;
        $is_owner = false;
        $can_view = false;
        $can_edit = false;
        $can_withdraw = false;
        $can_resubmit = false;
        $can_delete = false;
        $can_change_organization = false;

        if ($role == 's') {
            $can_submit = true;

            if ($core->timestamp >= $program_data['dl_student'])
            {
                $can_submit = false;

                if ($core->timestamp < $program_data['dl_mentor']) {
                    $late_submission = $this->any_late_submissions($program_data);
                    if ($late_submission)
                        $can_submit = true;
                }
            }
        } else if ($program_data['is_active'] && ($role == 'g' || $role == 'r' || $role == 'x')) {
            if ($core->timestamp <= $program_data['dl_student']) {
                $can_apply_student = true;
                $can_apply_mentor = true;
            } else if ($core->timestamp <= $program_data['dl_mentor']) {
                $can_apply_student = $this->any_late_submissions($program_data);
                $can_apply_mentor = true;
            } else {
                $can_apply_student = false;

                // In theory dl_mentor is when the program is announced, and no
                // projects should have been approved without a mentor - in practice
                // we might still need to sign up a mentor (or a replacement mentor)
                // so they can upload contracts, etc.
                //$can_apply_mentor = false;
                $can_apply_mentor = true;
            }
        }

        if ($project_data !== null) {
            $sql = "SELECT COUNT(*) AS count " .
                   "FROM {$db->prefix}projects prj " .
                   "LEFT JOIN {$db->prefix}participants prt " .
                   "ON prj.id = prt.project_id " .
                   "WHERE prj.id = :project_id " .
                   "AND prt.username = :username " .
                   "AND (prt.role = 's' " .
                   "OR (prt.role = 'm' " .
                   "AND prj.is_accepted = 1))";
            $row = $db->query($sql,
                              array('project_id' => $project_data['id'],
                                    'username' => $user->username),
                              true);

            $is_owner = $row['count'] > 0;

            $can_view = $user->is_admin || $role =='m' || $is_owner;

            if ($user->is_admin) {
                $can_edit = true;
                $can_change_organization = true;
                $can_withdraw = $project_data['is_withdrawn'] == 0;
                $can_resubmit = $project_data['is_withdrawn'] == 1;
                $can_delete = true;
            } else if ($is_owner && $role == 's') {
                if ($core->timestamp >= $program_data['dl_mentor']) {
                    // Past the selection deadline, students cannot edit anything,
                    // unless a special flag is checked by the admin for the project.
                    $can_edit = $project_data['late_edit'] != 0;

                    $can_change_organization = false;
                    $can_withdraw = $project_data['is_withdrawn'] == 0;
                    $can_resubmit = false;
                    $can_delete = false;
                } else if ($core->timestamp > $program_data['dl_student']) {
                    $can_edit = true;

                    // Projects submitted to organizations that are no longer taking
                    // new submissions can't be moved by the student to a different
                    // organization, because they can't be moved back.
                    if (!$can_submit) {
                        $can_change_organization = false;
                    } else if ($late_submission) {
                        $organization_data = $cache->get_organization_data($project_data['organization_id']);
                        $can_change_organization = $organization_data['late_submission'] != 0;
                    }

                    $can_withdraw = $project_data['is_withdrawn'] == 0;
                    $can_resubmit = $project_data['is_withdrawn'] == 1;
                    $can_delete = false;
                } else {
                    $can_edit = true;
                    $can_change_organization = true;
                    $can_withdraw = false;
                    $can_resubmit = false;
                    $can_delete = true;
                }
            }
        }

        $this->can_apply_student = $can_apply_student;
        $this->can_apply_mentor = $can_apply_mentor;
        $this->can_submit = $can_submit;
        $this->late_submission = $late_submission;
        $this->is_owner = $is_owner;
        $this->can_view = $can_view;
        $this->can_edit = $can_edit;
        $this->can_withdraw = $can_withdraw;
        $this->can_resubmit = $can_resubmit;
        $this->can_delete = $can_delete;
        $this->can_change_organization = $can_change_organization;
    }
}

function get_project_permissions($program_data, $role, $project_data) {
    return new ProjectPermissions($program_data, $role, $project_data);
}

function get_role_string($role, $has_project) {
  global $lang;

  $role_key = ($has_project and $role == 's') ? 'role_s_accepted' : ('role_' . $role);
  return $lang->get($role_key);
}

function escapenewlines($text) {
    $text = str_replace("\n", "&#10;", $text);
    $text = str_replace("\r", "&#13;", $text);
    return $text;
}

?>