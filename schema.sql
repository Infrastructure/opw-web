CREATE TABLE `opw_cron` (
  `timestamp` int(11) unsigned NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `opw_cron` (
  `timestamp`
) VALUES (0);

/* alter table opw_programs add `contracts_active` tinyint(1) NOT NULL DEFAULT 0 ; */
/* alter table opw_programs add `application_prefill` mediumtext NOT NULL DEFAULT '' ; */
CREATE TABLE `opw_programs` (
  `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext DEFAULT '',
  `application_prefill` mediumtext DEFAULT '',
  `start_time` int(11) unsigned NOT NULL,
  `end_time` int(11) unsigned NOT NULL,
  `dl_student` int(11) unsigned NOT NULL,
  `dl_mentor` int(11) unsigned NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  `contracts_active` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `opw_contracts` (
  `id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT,
  `program_id` mediumint(6) unsigned NOT NULL,
  `role` char(1) NOT NULL DEFAULT 's',
  `uploader` varchar(255) NOT NULL,
  `upload_time` int(11) unsigned NOT NULL,
  `contents` MEDIUMBLOB NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`program_id`) REFERENCES `opw_programs`(`id`),
  KEY (`program_id`, `id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `opw_organizations` (
  `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `program_id` mediumint(6) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `late_submission` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`program_id`) REFERENCES `opw_programs`(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/* alter table opw_projects add `late_edit` varchar(1) NOT NULL DEFAULT 0 ; */
CREATE TABLE `opw_projects` (
  `id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext,
  `program_id` mediumint(6) unsigned NOT NULL,
  `organization_id` mediumint(6) unsigned,
  `is_accepted` tinyint(1) NOT NULL DEFAULT -1,
  `is_complete` tinyint(1) NOT NULL DEFAULT 0,
  `is_withdrawn` tinyint(1) NOT NULL DEFAULT 0,
  `late_edit` tinyint(1) NOT NULL DEFAULT 0,
  `ranking` float NOT NULL DEFAULT -1,
  /* 'n' - No contribution
   * 'y' - No contribution, will not accept
   * 'c' - Contribution, under review
   * 'x' - Contribution, will not accept
   * 'g' - Will accept for GSoC
   * 'w' - Want to accept, need funding
   * 'f' - Will accept, have funding
   */
  `org_opinion` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`program_id`) REFERENCES `opw_programs`(`id`),
  FOREIGN KEY (`organization_id`) REFERENCES `opw_programs`(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `opw_participants` (
  `id` mediumint(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `project_id` mediumint(10) NOT NULL,
  `program_id` mediumint(6) unsigned NOT NULL,
  `role` char(1) NOT NULL DEFAULT 's',
  `passed` tinyint(1) DEFAULT -1,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`project_id`) REFERENCES `opw_projects`(`id`),
  FOREIGN KEY (`program_id`) REFERENCES `opw_programs`(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/* alter table opw_roles add `contract_approved` tinyint(1) NOT NULL DEFAULT 0 ; */
/* alter table opw_roles add `contract_accepted_time` int(11) NOT NULL DEFAULT 0 ; */
/* alter table opw_roles add `contract_id` mediumint(10) unsigned ; */
/* alter table opw_roles add foreign key (`contract_id`) references opw_contracts(id) ; */
/* alter table opw_roles add `contract_entered_name` varchar(255) NOT NULL DEFAULT '' ; */
/* alter table opw_roles add `contract_accepted_from` varchar(255) NOT NULL DEFAULT '' ; */
CREATE TABLE `opw_roles` (
  `id` mediumint(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `program_id` mediumint(6) unsigned NOT NULL,
  `organization_id` mediumint(6) unsigned,
  `role` char(1) NOT NULL DEFAULT 's',
  `contract_approved` tinyint(1) NOT NULL DEFAULT 0,
  `contract_accepted_time` int(11) unsigned NOT NULL DEFAULT 0,
  `contract_accepted_from` varchar(255) NOT NULL DEFAULT '',
  `contract_entered_name` varchar(255) NOT NULL DEFAULT '',
  `contract_id` mediumint(10) unsigned,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`program_id`) REFERENCES `opw_programs`(`id`),
  FOREIGN KEY (`organization_id`) REFERENCES `opw_organizations`(`id`),
  FOREIGN KEY (`contract_id`) REFERENCES `opw_contracts`(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `opw_session` (
  `username` varchar(255) NOT NULL DEFAULT '',
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `sid` varchar(40) NOT NULL DEFAULT '',
  `timestamp` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `opw_bans` (
  `username` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `opw_queue` (
  `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `program_id` mediumint(6) unsigned NOT NULL,
  `deadline` tinyint(1) DEFAULT 0,
  `complete` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`program_id`) REFERENCES `opw_programs`(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `opw_identities` (
  `provider` varchar(20) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `emailVerified` tinyint(1) DEFAULT 0,
  `profileUrl` varchar(255) NOT NULL,
  PRIMARY KEY (`provider`, `identifier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `opw_profiles` (
  `username` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `emailVerified` tinyint(1) DEFAULT 0,
  `websiteUrl` varchar(255) NOT NULL DEFAULT '',
  `is_admin` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*
alter table opw_attachments add `is_contract` tinyint(1) NOT NULL DEFAULT 0;
alter table opw_attachments add `program_id` mediumint(6) unsigned NOT NULL;
alter table opw_attachments add `uploader` varchar(255) NOT NULL;
create index uploader on opw_attachments (uploader) ;
alter table opw_attachments add foreign key (`project_id`) references opw_projects (`id`) ;
alter table opw_attachments add foreign key (`program_id`) references opw_programs (`id`) ;
update opw_attachments set uploader = (select username from opw_participants p where p.project_id = opw_attachments.project_id and p.role = 's');
update opw_attachments set program_id = (select p.program_id from opw_projects p where p.id = opw_attachments.project_id);
*/

CREATE TABLE `opw_attachments` (
  `id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT,
  `program_id` mediumint(6) unsigned NOT NULL,
  `project_id` mediumint(10) unsigned not NULL,
  `uploader` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `size` mediumint(10) unsigned NOT NULL,
  `data` MEDIUMBLOB NOT NULL,
  `is_contract` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`project_id`) REFERENCES `opw_projects`(`id`),
  FOREIGN KEY (`program_id`) REFERENCES `opw_programs`(`id`),
  KEY (`uploader`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
