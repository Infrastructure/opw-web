<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

$lang_data = array(
    /* Basic i18n */
    'html_direction'        => 'ltr',

    /* Site */
    'site_title'            => "Outreachy - Free and Open Source Software internships for women (cis and trans), trans men, and genderqueer people",
    'site_desc'             => "Outreachy helps women (cis and trans), trans men, and genderqueer people get involved in free and open source software. We provide a supportive community for beginning to contribute any time throughout the year and offer focused internship opportunities twice a year with a number of free software organizations.",

    'csrf_not_logged_in'    => 'Please hit back, log in, and try again.',
    'csrf_mismatch'         => 'Please hit back, reload the page, and try again.',
    'javascript_disabled'   => 'Javascript must be enabled for correct operation of this site.',

    /* Global keys */
    'kde_links'             => 'KDE Links',
    'not_logged_in'         => 'You are not logged in',
    'logged_in_as'          => 'Logged in as [[username]]',
    'log_in'                => 'Log in',
    'log_out'               => 'Log out',
    'administration'        => 'Administration',
    'actions'               => 'Actions',
    'submit_proposal'       => 'Submit an application',
    'manage_programs'       => 'Manage programs',
    'manage_organizations'  => 'Manage organizations',
    'manage_bans'           => 'Manage user bans',
    'navigation'            => 'Navigation',
    'view_all_progms'       => 'View all programs',
    'view_active_progms'    => 'View active programs',
    'view_archives'         => 'View archived projects',
    'view_rejected'         => 'View rejected projects',
    'accepted_projects'     => 'Accepted projects',
    'view_my_projects'      => 'View my mentored projects',
    'approve_proposal'      => 'Approve proposals',
    'approve_mentors'       => 'Approve mentors',
    'view_participants'     => 'View participants',
    'edit_templates'        => 'Edit email templates',
    'mandatory_all'         => 'Please fill in all the fields',
    'err_mandatory_fields'  => 'Please fill in all the mandatory fields',
    'delete'                => 'Delete',
    'yes'                   => 'Yes',
    'no'                    => 'No',
    'program_title'         => 'Program title',
    'project_title'         => 'Project title',
    'organization'          => 'Organization',
    'description'           => 'Description',
    'application_prefill'   => 'Initial application text',
    'no_programs'           => 'There are no programs to display',
    'no_organizations'      => 'There are no organizations to display',
    'error_occurred'        => 'An error occurred while processing your request',
    'confirm_deletion'      => 'Confirm deletion',
    'edit'                  => 'Edit',
    'save'                  => 'Save',
    'cancel'                => 'Cancel',
    'username'              => 'Username',
    'openid'                => 'OpenID',
    'sending_status'        => 'Sending status mails for project',
    'sending_result'        => 'Sending result mails for project',
    'status_ok'             => '(Status OK)',
    'status_error'          => '(Status ERROR)',
    'available'             => 'available',
    'unavailable'           => 'unavailable',
    'debug_render'          => 'Rendered in %ss',
    'debug_queries'         => 'DB queries: %s',
    'debug_users'           => 'User(s) online: %s',
    'debug_caching'         => 'Caching is %s',
    'debug_email'           => 'Mail service is %s',
    'approve'               => 'Approve',
    'reject'                => 'Reject',
    'accepted'              => 'Accepted',
    'rejected'              => 'Rejected',
    'withdrawn'             => 'Withdrawn',

    /* Homepage */
    'welcome_homepage'      => 'This site is for archival purposes only.',
    'home'                  => 'Home',

    /* Module: login */
    'reset'                 => 'Reset',
    'enter_openid'          => 'Please enter your OpenID',
    'enter_user_pw'         => 'Please enter your username and password',
    'login_error'           => 'Login failed! Please notify the <a href="mailto:webmaster@gnome.org">' .
                               'webmaster</a> if the problem persists',
    'iko_credentials'       => 'Log into [[site_name]] using OpenID:',
    'login_with_openid'     => 'Log into [[site_name]] using OpenID:',
    'login_with_google'     => 'Log into [[site_name]] using your Google account:',
    'login_with_facebook'   => 'Log into [[site_name]] using your Facebook account:',
    'iko_credentials'       => 'Log into [[site_name]] using OpenID:',
    'password'              => 'Password',
    'create_account'        => 'Create a new account',
    'create_account_exp'    => 'Don\'t have an account? In order to log into [[site_name]], you\'ll need an OpenID account.',
    'register_iko'          => 'Register on KDE Identity',
    'account_banned'        => 'Your account has been banned by an administrator',

    /* Module: view_programs */
    'select_program'        => 'Select a program to continue',
    'view_inactive_progms'  => 'View inactive programs',
    'inactive'              => 'inactive',

    /* Module: manage_programs */
    'add_program'           => 'Add new program',
    'edit_program'          => 'Edit program',
    'active'                => 'Active',
    'start_date'            => 'Start date',
    'end_date'              => 'End date',
    'student_contract'      => 'Intern agreement',
    'mentor_contract'       => 'Mentor agreement',
    'sample_contract_link'  => 'Link to sample agreement',
    'enable_contracts'      => 'Users can accept agreements',
    'reject_undecided'      => 'Reject all applications not marked as accepted',
    'dl_student'            => 'Application deadline',
    'dl_mentor'             => 'Selection deadline',
    'show_deadlines'        => 'Show deadlines',
    'form_notice'           => 'All fields marked with * are mandatory. All times are in [[timezone]].',
    'invalid_date'          => 'Please enter a valid start and end date',
    'invalid_deadlines'     => 'Please enter valid participant and mentor application deadlines',
    'confirm_program_del'   => 'If you delete this program, all projects associated with it will also get deleted. ' .
                               'Do you want to continue?',

    /* Module: manage_organizations */
    'add_organization'      => 'Add new organization',
    'edit_organization'     => 'Edit organization',
    'organization_title'    => 'Organization title',
    'late_submissions'      => 'Late submissions?',
    'confirm_organization_del' => 'If you delete this organization, all projects associated with it will also get deleted. ' .
                                  'Do you want to continue?',

    /* Module: programs_home */
    'login_to_participate'  => 'Log in to continue',
    'program_started'       => 'This program isn\'t accepting new participants',
    'apply_student'         => 'Apply to participate',
    'apply_mentor'          => 'Apply to mentor',
    'will_mentor_again'     => 'Will mentor again',
    'home'                  => 'Home',
    'my_profile'            => 'My profile',
    'view_submissions'      => 'View my submissions',
    'create_project'        => 'Create new project',
    'mentor_project_sel'    => 'Select project to mentor',
    'late_submission_notice' => 'The general application deadline has passed, however some organizations are still accepting applications.',
    'cancel_mentor'         => 'Cancel mentor application',
    'view_proposals'        => 'View project proposals',
    'resign_program'        => 'Resign from this program',
    'role_student_applied'  => 'You are applying for this program',
    'role_student_accepted' => 'You were accepted to this program!',
    'role_student_rejected' => 'You were not accepted for this program. Please continue working with your organization and apply in a future round.',
    'role_mentor'           => 'You are a mentor for this program',
    'role_resigned'         => 'You have resigned from this program and cannot participate again',
    'role_rejected'         => 'Your mentorship application has been declined',
    'role_intermediate'     => 'Your mentor application for this program is awaiting admin approval',
    'view_accepted'         => 'View accepted projects',
    'student_dl_info'       => 'Application deadline: [[dl_student]]',
    'mentor_dl_info'        => 'Selection decisions posted: [[dl_mentor]]',
    'to'                    => 'to',

    /* Module: manage_projects */
    'manage_projects'       => 'View applications',
    'go'                    => 'Go',
    'no_projects_for_organization' => 'No projects were found for this organization',
    'applicant'             => 'Applicant',
    'manage'                => 'Rank',
    'status'                => 'Status',
    'pending'               => 'Pending',
    'opinion_n'             => 'No contribution',
    'opinion_y'             => 'No contribution, will not accept',
    'opinion_c'             => 'Contribution, under review',
    'opinion_t'             => 'Contribution, full-time commitments',
    'opinion_x'             => 'Contribution, will not accept',
    'opinion_g'             => 'Will accept for GSoC',
    'opinion_w'             => 'Want to accept, need funding',
    'opinion_f'             => 'Will accept, have funding',
    'multiple_projects'     => 'Multiple',
    'multiple_projects_submit' => 'Student has submitted multiple projects',
    'multiple_projects_accept' => 'Organizations are planning to accept multiple projects from this student',


    /* Module: view_projects */
    'edit_project'          => 'Edit project',
    'how_to_submit'         => 'Please answer all the questions in the application form below. ' .
                               'Please take a look at the ' .
                               '<a href="https://wiki.gnome.org/Outreachy#Submit_an_Application">' .
                               'application instructions</a> for more information. ' .
                               'We recommend editing your answers locally and pasting ' .
                               'them in here because it can be a long process and ' .
                               'we would not want you to risk losing changes.',
    'attachment_notice'     => 'After you submit an initial version of your application, you will be able ' .
                               'to upload attachments, such as your resume or any other relevant document. ' .
                               'Submitting a resume is optional.',

    'project_description'   => 'Application text',
    'proposal_submitted'    => 'Your proposal has been submitted successfully',
    'mentor_submitted'      => 'You have been successfully added as a project mentor for this project',
    'mentor_removed'        => 'You are no longer a project mentor for this project',
    'project_updated'       => 'Project updated successfully',
    'project_home'          => 'Program home',
    'project_complete'      => 'Project complete?',
    'late_editing_allowed'  => 'Intern can edit proposal past selection deadline?',
    'student_result'        => 'Participant result',
    'passed'                => 'Passed',
    'failed'                => 'Failed',
    'undecided'             => 'Undecided',
    'confirm_project_del'   => 'Deletion of a project is irreversible. Are you sure you want to continue?',
    'withdrawn_notice'      => 'This application is withdrawn and will not receive consideration.',
    'withdraw'              => 'Withdraw application',
    'resubmit'              => 'Resubmit application',
    'confirm_withdraw'      => 'Confirm application withdrawal',
    'confirm_withdraw_exp'  => 'Are you sure you want to withdraw this application?',
    'confirm_project_del'   => 'Deletion of a project is irreversible. Are you sure you want to continue?',
    'mentor_project'        => 'Willing to mentor',
    'mentor_project_additional' => 'Add myself as additional mentor',
    'mentor_remove'         => 'Remove myself as mentor',
    'view_project'          => 'View project details',
    'project_accepted'      => 'Project accepted?',
    'your_projects'         => 'Your projects',
    'proposed_projects'     => 'Proposed projects',
    'rejected_projects'     => 'Rejected projects',
    'student'               => 'Applicant',

    'select_organization'   => '- select organization -',
    'other'                 => 'Other',
    'apply'                 => 'Apply',

    'mentor'                => 'Mentor',
    'mentors'               => 'Mentors',
    'no_projects'           => 'No projects were found in this category',
    'new_mentor'            => 'New mentor',
    'new_mentor_exp'        => 'Leave blank if you do not wish to change the mentor; separate multiple mentors with spaces; use - to remove all mentors',
    'new_mentor_student'    => 'The mentor you have selected is a participant in this program',
    'new_student'           => 'New applicant',
    'new_student_exp'       => 'Leave blank if you do not wish to change the applicant',
    'new_student_mentor'    => 'The applicant you have selected is a mentor for this program',
    'admin'                 => 'Admin',
    'approve_project'       => 'Approve this project',
    'reject_project'        => 'Reject this project',
    'subject_status'        => '[[site_name]]: Status of your submission',
    'subject_result'        => '[[site_name]]: Result of your project',
    'no_mentor'             => '<em>No mentor assigned</em>',
    'mentor_subject'        => 'New mentor request awaiting approval',
    'subscribe_student'     => '<b>Important:</b> It is essential that you subscribe to the <a href="https:' .
                               '//mail.kde.org/mailman/listinfo/kde-soc" target="_blank">KDE SoC Mailing List' .
                               '</a>, if you haven\'t done it already.',
    'subscribe_mentor'      => '<b>Important:</b> It is essential that you subscribe to the <a href="https:' .
                               '//mail.kde.org/mailman/listinfo/kde-soc-mentor" target="_blank">KDE SoC ' .
                               'Mentor Mailing List</a>, if you haven\'t done it already.',
    'confirm_resign'        => 'Confirm resignation',
    'confirm_resign_exp_delete'   => 'Are you sure you want to resign? Your submissions will be deleted.',
    'confirm_resign_exp_withdraw' => 'Are you sure you want to resign? Your submissions will be marked as withdrawn.',
    'confirm_resign_mentor_exp' => 'Are you sure you want to resign? You will be removed from any projects where you are currently listed as the mentor.',
    'cannot_resign'         => 'Cannot resign',
    'cannot_resign_mentor_exp' => 'Cannot resign as a mentor since you are currently mentoring accepted projects. Please contact an adminstrator to arrange for your projects to be transferred to another mentor.',

    /* Module: user_profile */
    'user_profile'          => 'User profile',
    'full_name'             => 'Full name',
    'email'                 => 'Email address',
    'website'               => 'Website',
    'verified'              => '(verified)',
    'unverified'            => '(unverified)',
    'previous_page'         => 'Previous page',
    'user_avatar'           => 'User avatar',
    'user_404'              => 'The requested user could not be found',
    'contact_user'          => 'Contact user',
    'full_profile'          => 'View full profile',
    'site_admin'            => 'Site admin',
    'log_in_identity'       => 'Log-in Identity',
    'profile_url'           => 'Profile page',
    'contract_documents'    => 'Contracts',
    'contract_approved'     => 'Contract approved',
    'upload_contract'       => 'Upload a contract document',
    'approve_contract'      => 'Approve contract',
    'unapprove_contract'    => 'Unapprove contract',
    'view_contract'         => 'View agreement',
    'review_contract'       => 'Review agreement',
    'contract_accepted'     => 'Agreement accepted',
    'contract_not_accepted' => 'Agreement not accepted',
    'by'                    => 'by',
    'visible_to_mentors'    => '(visible to mentors)',
    'visible_to_public'     => '(visible to public)',
    'visible_to_admins'     => '(visible to administrators)',
    'edit_user_profile'     => 'Edit user profile',

    /* Module: user_bans */
    'ban_user'              => 'Ban a user',
    'banned_users'          => 'Banned users',
    'unban'                 => 'Unban',
    'unban_user'            => 'Unban this user',
    'ban'                   => 'Ban',
    'no_bans'               => 'There are no banned users at the moment',

    /* Module: approve_mentors */
    'no_pending_mentors'    => 'There are no pending mentor applications',
    'mentor_name'           => 'Mentor username',

    /* Module: timeline */
    'application_start'     => 'Internships start',
    'student_appl_dl'       => 'Application deadline',
    'mentor_appl_dl'        => 'Selection decisions posted',
    'application_process'   => 'Application processing',
    'season_complete'       => 'Internships end',
    'students_coding'       => 'Internships continue',
    'off_season'            => 'Contributions on your own',
    'program_timeline'      => 'Program timeline',

    /* Module: edit_templates */
    'select_tpl'            => 'Select template',
    'load_tpl'              => 'Load template',
    'language'              => 'Language',
    'no_tpl_loaded'         => 'No template loaded. Select a template file from the list above and click on ' .
                               '\'Load template\' button to edit it.',
    'tpl_saved'             => 'Template saved successfully',
    'tpl_save_error'        => 'An error occurred while saving the template file',
    'save_tpl'              => 'Save template',
    'placeholders'          => 'Placeholders',
    'recepient'             => 'Recepient\'s full name',
    'program_name'          => 'Name of the program',
    'project_name'          => 'Name of the project',
    'project_url'           => 'Permanent URL for the project',
    'student_fname'         => 'Applicant\'s full name and e-mail address',
    'mentor_fname'          => 'Mentor\'s full name and e-mail address',

    /* Module: view_participants */
    'prog_participants'     => 'Program participants',
    'accepted_participants' => 'Accepted participants',
    'role'                  => 'Role',
    'role_s'                => 'Applicant',
    'role_s_accepted'       => 'Intern',
    'role_r'                => 'Resigned applicant',
    'role_x'                => 'Rejected mentor',
    'role_m'                => 'Mentor',
    'role_i'                => 'Unapproved mentor',
    'role_g'                => 'Guest',
    'uploaded'              => 'Uploaded',
    'no_participants'       => 'This programs has no participants',
    'projects'              => 'Projects',
    'contract_archive'      => 'Contract archive',

    /* Module: notifications */
    'notification_mails'    => 'Notification mails',
    'mail_queue'            => 'Queued emails',
    'mail_queue_exp'        => 'Each program has two types of emails: acceptance emails and result emails. Acceptance ' .
                               'emails are sent out to applicants and mentors only after the mentor deadline is reached ' .
                               'while result emails are sent out once the program has ended. Use the <b>Send notifications</b> ' .
                               'button to trigger these mails. No matter when you trigger them, the mails will be sent ' .
                               'only past these deadlines.',
    'acceptance_mails'      => 'Acceptance emails',
    'result_mails'          => 'Result emails',
    'queue_empty'           => 'There are no items in the queue',
    'send_notifications'    => 'Send notifications',
    'sendmail_output'       => 'Sendmail output',
    'processing_program'    => 'Processing program #',
    'program_no_mail'       => 'No mails were sent for this program',

    /* Module: attachment */
    'add_attachment'            => "Add attachment",
    'attachment'                => "Attachment",
    'attachment_description'    => "Description",
    'upload_description_needed' => 'Please supply a description for the attachment',
    'upload_no_file'            => 'Please select a file to upload',
    'upload_failed'             => 'File upload failed',
    'upload_too_large'          => 'File too large (maximum size is 1MB)',
    'upload_unknown_type'       => 'File type is unsupported (supported: PDF, ODT, TXT)',
    'confirm_delete_attachment' => 'Are you sure that you want to delete the attachment?',
    'upload_contract_document'  => 'Upload contract document',
    'document'                  => 'Document',
    'contract_sign_message'     => 'Make sure that you have <span class="alert-emphasis">initialed each page</span> and signed and dated the last page',

    /* Module: contract */
    'review_contract_notice_student'   => 'Please review the following terms of participation as an intern in Outreachy. If they are acceptable to you, please type your full name then click on "Accept Agreement" at the bottom of the page.',
    'review_contract_notice_mentor'    => 'Please review the following terms of participation as a mentor in Outreachy. If they are acceptable to you, please type your full name then click on "Accept Agreement" at the bottom of the page.',
    'view_contract_notice'      => 'You accepted the following agreement on: ',
    'contract_from_notice'      => ', from IP address: ',
    'view_contract_notice_name' => 'You gave your full name as',
    'type_your_name'            => 'Please type your full name',
    'accept_contract'           => "Accept Agreement",

    'edit_contract'             => "Edit agreement",
    'intern_contract'           => "Intern agreement",
    'mentor_contract'           => "Mentor agreement",
    'contract_preview_message'           => "This is a preview and not yet saved.",
    'contract_edit_message'     => "Please enter the text of the agreement below using <a href='http://daringfireball.net/projects/markdown/syntax'>Markdown</a> syntax",
    'formatted_result'          => "Formatted result",
    'preview'                   => "Preview",
    'via'                       => 'via'
);

