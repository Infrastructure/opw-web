How to Install
==============

* Copy config.sample.php to config.php and fill in the values as you proceed
* Create a MySQL database and initialize it using `schema.sql`

