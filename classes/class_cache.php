<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

class cache
{
    // Global vars
    var $is_available;
    var $lite;

    // Constructor
    function __construct()
    {
        global $config;

        if (!$config->enable_cache) {
            $this->is_available = false;
            return;
        }

        @include('Cache/Lite.php');

        if (class_exists('Cache_Lite'))
        {
            include_once('PEAR.php');
            $cache_path = realpath('./cache') . '/';

            // Set the caching options
            $options = array(
                'cacheDir'               => $cache_path,
                'lifeTime'               => 7200,
                'automaticSerialization' => true,
            );

            // Inistantiate the cache objects
            $this->lite = new Cache_Lite($options);
            $this->is_available = !@PEAR::isError($this->lite) && is_writable($cache_path);
        }
        else
        {
            $this->is_available = false;
        }
    }

    // Gets a value from the cache
    function get($key, $group = 'default')
    {
        if ($this->is_available)
        {
            return $this->lite->get($key, $group);
        }
        else
        {
            return false;
        }
    }

    // Saves a value to the cache
    function put($key, $data, $group = 'default')
    {
        if ($this->is_available)
        {
            return $this->lite->save($data, $key, $group);
        }
        else
        {
            return false;
        }
    }

    // Deletes data from the cache
    function remove($key, $group = 'default')
    {
        if ($this->is_available)
        {
            return $this->lite->remove($key, $group);
        }
        else
        {
            return false;
        }
    }

    // Purges the cache (for a specific group)
    function purge($groups = array('default'))
    {
        if (!is_array($groups))
        {
            $groups = array($groups);
        }

        if ($this->is_available)
        {
            $status = true;

            foreach ($groups as $group)
            {
                $status = $status && $this->lite->clean($group);
            }

            return $status;
        }
        else
        {
            return false;
        }
    }

    // Utility functions - really should go elsewhere
    function get_program_data($id)
    {
        global $db;

        $program_data = $this->get("program_{$id}", 'programs');

        if (!$program_data)
        {
            $sql = "SELECT * FROM {$db->prefix}programs " .
                       "WHERE id = ?";
            $program_data = $db->query($sql, $id, true);

            $this->put("program_{$id}", $program_data, 'programs');
        }

        return $program_data ? $program_data : null;
    }

    function get_organization_data($id)
    {
        global $db;

        $organization_data = $this->get("organization_{$id}", 'organizations');

        if (!$organization_data)
        {
            $sql = "SELECT * FROM {$db->prefix}organizations " .
                       "WHERE id = ?";
            $organization_data = $db->query($sql, $id, true);

            $this->put("organization_{$id}", $organization_data, 'organizations');
        }

        return $organization_data ? $organization_data : null;
    }

    function get_project_data($id)
    {
        global $db;

        $project_data = $this->get("project_{$id}", 'projects');

        if (!$project_data)
        {
            $sql = "SELECT * FROM {$db->prefix}projects " .
                       "WHERE id = ?";
            $project_data = $db->query($sql, $id, true);

            $this->put("project_{$id}", $project_data, 'projects');
        }

        return $project_data ? $project_data : null;
    }
}
?>
