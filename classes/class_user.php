<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

require_once(__DIR__ . "/../auth/Hybrid/Auth.php");

class user
{
    // Global vars
    var $username;
    var $sid;
    var $is_logged_in;
    var $is_admin;
    var $max_age;

    // Constructor
    function __construct()
    {
        global $config, $gsod;

        $this->_hybridauth = null;
        $this->username = null;
        $this->sid = null;
        $this->is_admin = false;
        $this->is_logged_in = false;
        $this->max_age = time() - 24 * 60 * 60;
        $this->_role_data = array();

        if (!isset($config->server_secret))
        {
            $title   = 'Server secret not set';
            $message = 'Please generate a server secret and add it to config.php.';
            $gsod->trigger($title, $message);
        }
    }

    function hybridauth() {
        global $config;

        if (!isset($this->_hybridauth)) {
            $config = array(
                // "base_url" the url that point to HybridAuth Endpoint (where index.php and config.php are found)
                // Trailing slash here is mandatory
                "base_url" => $config->site_url . "auth/",

                "providers" => array (
                     "OpenID" => array (
                         "enabled" => $config->auth_openid_enabled,
                     ),
                     "Google" => array (
                         "enabled" => true,
                         "keys" => array (
                             "id" => $config->auth_google_id,
                             "secret" => $config->auth_google_secret
                         ),
                         "scope" => "openid email profile" ,
                          // We need to have offline access because HybridAuth's google provider doesn't
                          // deal well with the case where it has an expired acess token and no refresh
                          // token. (A refresh token is only given to offline clients.)
                         "access_type" => "offline"
                     ),
                     "Facebook" => array (
                         "enabled" => true,
                         "keys" => array (
                              "id" => $config->auth_facebook_id,
                              "secret" => $config->auth_facebook_secret
                         ),
                         "scope" => "email"
                     ),
                 ),
                 "debug_mode" => $config->auth_debug_enabled,
                 "debug_file" => $config->auth_debug_file
            );

            $this->_hybridauth = new Hybrid_Auth($config);
        }

        return $this->_hybridauth;
    }

    // Method for creating a new session
    function create_session($username, $is_admin = false)
    {
        global $core, $db;

        $fp = fopen('/dev/urandom', 'rb');
        $bytes = fread($fp, 16);
        $this->sid = bin2hex($bytes);
        fclose($fp);

        $admin_flag = $is_admin ? 1 : 0;

        $params = array('sid' => $this->sid,
                        'is_admin' => $admin_flag,
                        'timestamp' => $core->timestamp,
                        'username' => $username);

        // Update/insert the session ID into the DB
        $sql = "UPDATE {$db->prefix}session " .
               "SET sid = :sid, " .
               "    is_admin = :is_admin, " .
               "    timestamp = :timestamp " .
               "WHERE username = :username";
        $db->query($sql, $params);

        if ($db->affected_rows() <= 0)
        {
            $sql = "INSERT INTO {$db->prefix}session " .
                   "(username, is_admin, sid, timestamp) " .
                   "VALUES (:username, :is_admin, :sid, :timestamp)";
            $db->query($sql, $params);
        }

        // Save username and session ID to a cookie
        $core->set_cookie('username', $username);
        $core->set_cookie('session_id', $this->sid);
    }

    // Method for verifying the current session
    function verify()
    {
        global $core, $db;

        // Read username and session ID from a cookie
        $username = $core->variable('username', '', true);
        $sid = $core->variable('session_id', '', true);

        // Get current session data
        $sql = "SELECT * FROM {$db->prefix}session " .
               "WHERE username = ? " .
               "AND sid = ?";
        $row = $db->query($sql, array($username, $sid), true);

        if ($row !== false)
        {
            // Update the DB with current time
            $sql = "UPDATE {$db->prefix}session " .
                   "SET timestamp = {$core->timestamp} " .
                   "WHERE username = '{$username}' " .
                   "AND sid = '{$sid}'";
            $db->query($sql);

            // Set context data
            $this->username = $row['username'];
            $this->sid = $row['sid'];
            $this->is_admin = ($row['is_admin'] == 1);
            $this->is_logged_in = true;
        }
    }

    // Checks is a user is banned
    function is_banned($username)
    {
        global $db;

        // Check if an entry for the user exists in the ban table
        $sql = "SELECT COUNT(*) AS count " .
               "FROM {$db->prefix}bans " .
               "WHERE username = ?";
        $row = $db->query($sql, $username, true);

        // Return true if an entry was found
        return $row['count'] > 0;
    }

    // Gets total number of online users
    function online()
    {
        global $db;

        $sql = "SELECT COUNT(*) AS count " .
               "FROM {$db->prefix}session";
        $row = $db->query($sql, null, true);

        // Return the user count
        return $row['count'] > 0;
    }

    // Get details of the user from LDAP
    function get_details_ldap($username, $entries)
    {
        global $config, $db, $cache;

        $values = array();
        $entries = !is_array($entries) ? array($entries) : $entries;

        // Username is required
        if (empty($username)) {
            return false;
        }

        // Read user data from cache
        $crc = crc32($username . implode('', $entries));
        $cached = $cache->get($crc, 'users');
        if ($cached !== false) {
            return $cached;
        }

        //TODO
        $user_data = null;

        if ($user_data === null) {
            return false;
        }

        foreach ($entries as $entry) {
            $values[$entry] = (isset($user_data[0][$entry]))
                ? $user_data[0][$entry]
                : array('');
        }

        // Add values to cache
        $cache->put($crc, $values, 'users');

        return $values;
    }

    // Method for authenticating a user
    function login($username, $password)
    {
        global $config, $db;
        $is_admin = false;

        // Connect to the LDAP server

        /*TODO
        if (true) {
          $is_admin = true;
        }
         */

        /*TODO
        if (true) {
          // Create a new session for the user
          $this->create_session($username, $is_admin);

          // Authentication was successful
          return true;
        }
         */

        // Username was not found
        return false;
    }

    // Method for authenticating a user
    function login_openid($openid)
    {
        $hybridauth = $this->hybridauth();
        if ($hybridauth->authenticate("OpenID", array (
            'openid_identifier' => $openid,
            'hauth_return_to' => Hybrid_Auth::getCurrentUrl() . '&p=OpenID'
            ))) {
            return $this->finish_login('OpenID');
        } else {
            return false;
        }
    }

    // Method for authenticating a user
    function login_google()
    {
        $hybridauth = $this->hybridauth();
        if ($hybridauth->authenticate("Google", array (
            'hauth_return_to' => Hybrid_Auth::getCurrentUrl() . '&p=Google'
        ))) {
            return $this->finish_login('Google');
        } else {
            return false;
        }
    }

    // Method for authenticating a user
    function login_facebook()
    {
        $hybridauth = $this->hybridauth();
        if ($hybridauth->authenticate("Facebook", array (
            'scope' => 'email, basic_info',
            'hauth_return_to' => Hybrid_Auth::getCurrentUrl() . '&p=Facebook'
        ))) {
            return $this->finish_login('Facebook');
        } else {
            return false;
        }
    }

    function lookup_identity($provider, $identifier)
    {
        global $db;

        // Get current session data
        $sql = "SELECT username FROM {$db->prefix}identities " .
               "WHERE provider = :provider " .
               "AND identifier = :identifier";
        $row = $db->query($sql,
                          array('provider' => $provider,
                                'identifier' => $identifier),
                          true);

        if ($row !== false)
           return $row['username'];
        else
           return null;
    }

    function lookup_user($username)
    {
        global $db, $cache;

        $key = 'user_' . $username;
        $values = $cache->get($key, 'users');

        if ($values == false) {
            // Get current session data
            $sql = "SELECT * FROM {$db->prefix}profiles " .
                   "WHERE username = ?";
            $row = $db->query($sql, $username, true);
            if ($row === false) {
                return null;
            } else {
                $values = array();

                $values['is_admin'] = $row['is_admin'] != 0;
                $values['fullname'] = $row['fullname'];
                $values['websiteUrl'] = $row['websiteUrl'];
                $values['email'] = $row['email'];
                $values['emailVerified'] = $row['emailVerified'] != 0;

                $cache->put($key, $values, 'users');
            }
        }

        return $values;
    }

    function create_user($provider, $identifier, $profile)
    {
        global $db, $cache;

        // The OpenID provider sets displayName to a reversed lastname - firstname,
        // if unset. Detect this to avoid thinking we got a real DisplayName from
        // the provider.
        $badDisplayName = trim( $profile->lastName . " " . $profile->firstName );

        if ($profile->displayName != '' && $profile->displayName != $badDisplayName)
            $fullname = trim($profile->displayName);
        else if ($profile->firstName != '' && $profile->lastName != '')
            $fullname = trim($profile->firstName) . " " . trim($profile->lastName);
        else
            $fullname = '';

        // Derive the username first from the email; if not present from the display name
        $suffix = "";
        $email = $profile->email;
        if ($email != "") {
            $pos = strpos($email, "@");
            if ($pos === false)
                $username = $email;
            else
                $username = substr($email, 0, $pos);
        } else if ($fullname != '') {
            $username = $fullname;
        } else {
            $username = 'user';
            $suffix = 1;
        }

        // Sanitize resulting username
        $username = preg_replace('/[^A-Za-z0-9_.-]+/', '.', $username);
        $username = preg_replace('/^\./', '', $username);
        $username = preg_replace('/\.$/', '', $username);

        // If we sanitized everything away, go back to the extreme fallback
        if ($username == '') {
            $username = 'user';
            $suffix = 1;
        }

        // Now we need to make sure that everything is unique
        while ($this->lookup_user($username . $suffix)) {
            if ($suffix === "")
                $suffix = 2;
            else
                $suffix++;
        }

        $username = $username . $suffix;

        if ($profile->emailVerified != "") {
            $email = $profile->emailVerified;
            $emailVerified = 1;
        } else {
            $email = $profile->email;
            $emailVerified = 0;
        }

        # The profileUrl for the provider should be something verified
        if ($provider == 'OpenID')
            $profileUrl = $identifier;
        else
            $profileUrl = $profile->profileURL;

        $params = array('username' => $username,
                        'fullname' => $fullname,
                        'email' => $email,
                        'emailVerified' => $emailVerified,
                        'profileUrl' => $profileUrl,
                        'is_admin' => 0,
                        'provider' => $provider,
                        'identifier' => $identifier);

        $sql = "INSERT INTO {$db->prefix}profiles " .
                   "(username, fullname, email, emailVerified, websiteUrl, is_admin) " .
                   "VALUES (:username, :fullname, :email, :emailVerified, :profileUrl, :is_admin)";
        $db->query($sql, $params);

        $sql = "INSERT INTO {$db->prefix}identities " .
                   "(provider, identifier, username, fullname, email, emailVerified, profileUrl) " .
                   "VALUES (:provider, :identifier, :username, :fullname, :email, :emailVerified, :profileUrl)";
        $db->query($sql, $params);

        $cache->purge('users');

        return $username;
    }

    function finish_login($provider)
    {
        global $core;

        $hybridauth = $this->hybridauth();

        if ($provider == "OpenID" && $hybridauth->isConnectedWith("OpenID")) {
            $adapter = $hybridauth->getAdapter("OpenID");
            $profile = $adapter->getUserProfile();
        } else if ($provider == "Google" && $hybridauth->isConnectedWith("Google")) {
            $adapter = $hybridauth->getAdapter("Google");
            $profile = $adapter->getUserProfile();
        } else if ($provider == "Facebook" && $hybridauth->isConnectedWith("Facebook")) {
            $adapter = $hybridauth->getAdapter("Facebook");
            $profile = $adapter->getUserProfile();
        } else {
           return false;
        }

        $identifier = $profile->identifier;

        $username = $this->lookup_identity($provider, $identifier);

        $user_created = false;
        if ($username == null) {
            $username = $this->create_user($provider, $identifier, $profile);
            $user_created = true;
        }

        $info = $this->lookup_user($username);
        $this->create_session($username, $info['is_admin']);

        if ($user_created) {
            // The first time a user logs in, take them to their profile
            $username_url = urlencode($this->username);
            $core->redirect("?q=user_profile&u={$username_url}");
        }

        return true;
    }

    // Method for logging a user out
    function logout()
    {
        global $core, $db;

        $hybridauth = $this->hybridauth();
        $hybridauth->logoutAllProviders();

        // Get username and session ID from cookie
        $username = $core->variable('username', '', true);
        $sid = $core->variable('session_id', '', true);

        // Delete session cookies
        $core->unset_cookie('username');
        $core->unset_cookie('session_id');

        // Delete session data from the DB
        $sql = "DELETE FROM {$db->prefix}session " .
               "WHERE username = ? AND sid = ?";
        $db->query($sql, array($username, $sid));
    }

    // Gets the profile link for a user
    function profile($username, $return = false, $fullname = null)
    {
        global $core;

        if ($username == '-')
            return $username;

        if ($fullname === null) {
            $info = $this->lookup_user($username);
            if ($info === null)
                return htmlspecialchars($username);
            $fullname = $info['fullname'];
        }

        $username_url = urlencode($username);
        $profile_url = "?q=user_profile&u={$username_url}";

        if ($return)
        {
            $redir_url = urlencode($core->request_uri());
            $profile_url .= '&r=' . urlencode($redir_url);
        }

        return '<a href="' . htmlspecialchars($profile_url) . '">' . htmlspecialchars($fullname) . '</a>';
    }

    // Get the role and organization of the current user
    function get_role($program_id, &$role, &$organization) {
        global $cache, $db;

        $organization = null;
        $role = 'g';

        if ($this->username === null)
            return;

        // We frequently get the role from scratch for the same program ID;
        // even if the disk cache isn't on, we want to make sure to do this
        // only once per-session, so keep a local cache.

        if (isset($this->_roledata[$program_id]))
          $role_data = $this->_roledata[$program_id];
        else {
            $key = "role_{$this->username}_{$program_id}";
            $role_data = $cache->get($key, 'roles');
            if (!$role_data)
            {
                $sql = "SELECT role, organization_id FROM {$db->prefix}roles " .
                           "WHERE username = :username " .
                           "AND program_id = :id";

                $role_data = $db->query($sql,
                                        array('username' => $this->username,
                                              'id' => $program_id),
                                        true);
                $cache->put($key, $role_data, 'roles');
            }
            $this->_roledata[$program_id] = $role_data;
        }

        // Check if we have a role
        if ($role_data !== false)
        {
            $role = $role_data['role'];
            $organization = $role_data['organization_id'];
        }
    }

    // See if the current user is a mentor in any program (allows seeing user profile details)
    function isMentorAnyProgram() {
        global $db;

        if (!$this->is_logged_in)
            return false;

        $sql = "SELECT COUNT(*) as count FROM {$db->prefix}roles " .
                   "WHERE username = ? and role = 'm'";
        $row = $db->query($sql, $this->username, true);

        return $row['count'] > 0;
    }

    // Restricts a screen to a specific condition only
    function restrict($condition, $admin_override = false)
    {
        global $core;

        if (!$condition)
        {
            if (!$admin_override || ($admin_override && !$this->is_admin))
            {
                $core->redirect($core->path());
            }
        }
    }

    // Escapes the auth string in LDAP authentication
    function escape($string)
    {
        return str_replace(array('*', '\\', '(', ')'), array('\\*', '\\\\', '\\(', '\\)'), $string);
    }

    function csrf_token()
    {
        global $config;

        if (!$this->sid)
            return '';

        // The advantage of using this rather then just the session ID itself
        // is that if the token leaks (by an URL, say) an attacker won't be
        // able to use it without also getting the session ID.
        // Note that HMAC-MD5 is still considered very strong desite
        // attachs on MD5 - but no downside from using a better algorithm.
        return hash_hmac('sha256', $this->sid, $config->server_secret);
    }

    function check_csrf()
    {
        global $core, $lang;

        // The most likely reason for a CSRF prevention validation failure is
        // that the user logged out from a different browser tab, and either
        // is still logged out, or logged in again. The second most likely
        // reason is a site bug.

        // We require POST here - it would be possible to embed the CSRF token
        // in the URL for a GET, but GET requests shouldn't have side effects
        // in any case.

        if (!$this->sid ||
            $this->csrf_token() != $_POST['tok'])
        {
            header("HTTP/1.0 403 Forbidden");

            if (!$this->sid)
                echo $lang->get("csrf_not_logged_in");
            else
                echo $lang->get("csrf_mismatch");

            exit;
        }
    }
}
