<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

class db
{
    // Class wide variables
    var $dbh;
    var $prefix;
    var $hits;

    var $_TYPE_MAP = array(
        's' => PDO::PARAM_STR,
        'i' => PDO::PARAM_INT,
        'b' => PDO::PARAM_BOOL,
        'l' => PDO::PARAM_LOB
    );

    // Constructor
    function __construct()
    {
        $this->hits = 0;
    }

    // Function to initialize a db connection
    function connect()
    {
        global $gsod, $config;

        try
        {
            $dsn = "mysql:host={$config->db_host};dbname={$config->db_name}";
            if ($config->db_port != '')
                $dsn .= ";port={$config->db_port}";

            $this->dbh = new PDO($dsn, $config->db_username, $config->db_password);
            $this->prefix = $config->db_prefix;
            $this->_affected_rows = 0;
        }
        catch (PDOException $e)
        {
            $title   = 'Database error';
            $message = 'Database connection failed! Please check your DB settings.';
            $gsod->trigger($title, $message);
        }
    }

    function _bind_params($sql, $statement, $params)
    {
        if (is_array($params)) {
            if (array_key_exists(0, $params)) {
                foreach ($params as $key => $value) {
                    $statement->bindValue($key + 1, $value);
                }
            } else {
                preg_match_all('/:([A-Za-z_][A-Za-z_0-9]+)/', $sql, $matches);
                $bound_parameters = array();
                foreach ($matches[1] as $key) {
                    $bound_parameters[$key] = 1;
                }

                foreach ($params as $key => $value) {
                    if ($key[1] == ':') {
                        $type = $this->_TYPE_MAP[$key[0]];
                        $key = substr($key, 2);
                    } else {
                        $type = PDO::PARAM_STR;
                    }

                    if (array_key_exists($key, $bound_parameters))
                        $statement->bindValue($key, $value, $type);
                }
            }
        } else if (!is_null($params)) {
            $statement->bindValue(1, $params);
        }
    }

    // Function to return a recordset
    function query($sql, $params = null, $single = false)
    {
        try
        {
            global $gsod;

            $this->hits++;
            $recordset = array();

            if (stripos($sql, 'SELECT') === 0)
            {
                // Append limit to single row select query
                if ($single)
                {
                    $sql .= " LIMIT 1";
                }

                // Execute the query
                $statement = $this->dbh->prepare($sql);
                $this->_bind_params($sql, $statement, $params);
                $result = $statement->execute();
                $this->_affected_rows = 0;

                // Some error occurred
                if (!$result)
                {
                    $title    = 'Database error';
                    $error_info = $statement->errorInfo();
                    $message  = "Error: {$error_info[2]}<br />";
                    $message .= "Whole query: {$sql}";
                    $gsod->trigger($title, $message);
                }

                // Return the data
                if (!$single)
                    return $statement->fetchAll(PDO::FETCH_ASSOC);
                else
                    return $statement->fetch(PDO::FETCH_ASSOC);
            }
            else
            {
                $statement = $this->dbh->prepare($sql);
                $this->_bind_params($sql, $statement, $params);
                $result = $statement->execute();

                // Some error occurred
                if (!$result)
                {
                    $title    = 'Database error';
                    $error_info = $statement->errorInfo();
                    $message  = "Error: {$error_info[2]}<br />";
                    $message .= "Whole query: {$sql}";
                    $gsod->trigger($title, $message);
                }
                else
                {
                    $this->_affected_rows = $statement->rowCount();
                }
            }

            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    // Function to get the last inserted query ID
    function get_id()
    {
        return $this->dbh->lastInsertId();
    }

    // Function to check affected rows
    function affected_rows()
    {
        return $this->_affected_rows;
    }

    // Object destructor
    function __destruct()
    {
    }
}

?>