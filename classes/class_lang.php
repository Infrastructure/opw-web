<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

class lang
{
    // Class wide variables
    var $lang_name;
    var $lang_vars;

    // Constructor
    function __construct()
    {
        global $config, $core;

        $this->lang_name = $config->lang_name;
        $this->lang_vars = null;
        $this->lang_data = null;
    }

    function _init_lang_vars()
    {
        global $core, $config, $user;

        if ($this->lang_vars !== null)
            return;

        $this->lang_vars = array(
            'host' => htmlspecialchars($core->base_uri()),
            'site_name' => htmlspecialchars($config->site_name),
            'username' => htmlspecialchars($user->username),
            'timezone' => htmlspecialchars(date('T'))
        );
    }

    function _load()
    {
        global $config;

        if ($this->lang_data !== null)
            return;

        if (file_exists(realpath("lang/{$this->lang_name}.php")))
        {
            include("lang/{$this->lang_name}.php");
        }
        else
        {
            $title    = 'Language parser error';
            $message  = 'Error: Language file not found<br />';
            $message .= 'Verify that the language selected is present in the lang/ folder';
            $gsod->trigger($title, $message);
        }

        // Add default values
        $lang_data['lang_name'] = $this->lang_name;
        $lang_data['site_name'] = $config->site_name;
        $lang_data['site_copyright'] = $config->site_copyright;

        $this->lang_data = $lang_data;
    }

    function _replace($matches)
    {
        $k = $matches[1];
        if (isset($this->lang_data[$k]))
            return $this->parse_vars($this->lang_data[$k]);
        else
            return $k;
    }

    // Function to parse localization data
    function parse($data)
    {
        global $core, $user, $gsod, $config;

        $this->_load();
        $this->_init_lang_vars();

        // Replace localized strings; unknown strings are left as is
        return preg_replace_callback('/\{\{(.*?)\}\}/',
                                     array($this, '_replace'),
                                     $data);
    }

    function _replace_var($matches)
    {
        $k = $matches[1];
        if (isset($this->lang_vars[$k]))
            return $this->lang_vars[$k];
        else
            // Remove unknown placeholders
            return '';
    }

    // Parses language variables
    function parse_vars($data)
    {
        return preg_replace_callback('/\[\[(.*?)\]\]/',
                                     array($this, '_replace_var'),
                                     $data);
    }

    // Function to assign language variables
    function assign($data, $value = "")
    {
        $this->_init_lang_vars();

        if (!is_array($data) && $value)
        {
            $this->lang_vars[$data] = $value;
        }
        else
        {
            foreach ($data as $key => $value)
            {
                $this->lang_vars[$key] = $value;
            }
        }
    }

    // Function to return a localized phrase
    function get($key)
    {
        global $config, $core, $user;

        $this->_load();

        if (isset($this->lang_data[$key]))
        {
            $data = $this->lang_data[$key];

            // Parse placeholders
            $data = $this->parse_vars($data);

            // Return localized data
            return $data;
        }
        else
        {
            return $key;
        }
    }

    // Function to assign default variables
    function get_defaults()
    {
        global $config;

    }

    // Function to exclude a string from being treated as a key
    function escape(&$data)
    {
        $data = preg_replace('/\{\{(.*?)\}\}/', '&#123;&#123;$1&#125;&#125;', $data);
    }
}

?>
