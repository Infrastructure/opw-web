<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

/**
* In case if you're wondering, GSoD is, in fact, the Grey Screen of Death!
*/

class gsod
{
    // Method to trigger an error
    function trigger($title, $message)
    {
        global $config;
        // This needs to be hard coded, we can't depend on any of the class files
        ?>
            <html>
                <head>
                  <title><?php echo $config->site_name; ?> - Oops... Fatal Error!</title>
                    <style type="text/css">
                        body {background:#ccc; font-family:sans-serif; font-size:10pt; text-align: center;}
                        div {border:2px solid #20b2aa; border-radius:10px; padding:2em; margin:3em; background: #fff;}
                        h1 a {color: #20b2aa;}
                        p {margin: 2em 0;}
                        code {font-size: 8pt; color: gray;}
                    </style>
                </head>
                <body>
                    <div>
                        <h1><a href="<?php echo $config->site_url; ?>"><?php echo $config->site_name; ?></a></h1>
                        <br/>
                        <p style="color: #700;"><b>Oops... Fatal Error!</b></p>
                        <p>Please try again later.</p>
                        <br/>
                    </div>
                    <code>[<i><?php echo $title; ?></i>: <?php echo $message; ?>]</code>
                </body>
            </html>
        <?php
        exit;
    }
}

