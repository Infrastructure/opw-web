<?php
/**
* opw-web v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
* @copyright (c) 2013 GNOME Foundation. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

///NOTE: Unless mentioned explicitly, all settings are mandatory

// DB Hostname
$db_host = "";

// DB port (optional)
$db_port = "";

// DB name
$db_name = "";

// DB username (should have rw access)
$db_username = "";

// Password for DB user
$db_password = "";

// Table prefix
$db_prefix = "";

// Server secret key, used for CSRF protection, generate with: openssl rand -base64 18
$server_secret = '';

// Whether caching is turned on if Cache-Lite is installed; turn off for development
$enable_cache = true;

// Login authentication - see documentation on supported providers
// at http://hybridauth.sourceforge.net/userguide.html for how to
// sign-up for keys for providers that require them.
$auth_openid_enabled = true;

$auth_google_enabled = false;
$auth_google_id = '';
$auth_google_secret = '';

$auth_facebook_enabled = true;
$auth_facebook_id = '';
$auth_facebook_secret = '';

// The file must be web-server writable
$auth_debug_enabled = false;
$auth_debug_file = '';

// Site name
$site_name = "Outreach Program for Women";

// Site canonize url
$site_url = "https://opw.gnome.org/";

// Site copyright notice
$site_copyright = "&copy; KDE Webteam and GNOME Foundation";

// Webmaster's email address
$webmaster = "webmaster@gnome.org";

// Name of current skin
$skin_name = "Neverland";

// Currently active language
$lang_name = "en-gb";

// No. of items to display per page for lists
$per_page = 20;

// Show debug info in the footer
$show_debug = true;

// When recording the source IP for a request, how many
// remote addresses should be skipped
$skip_proxy_ip_count = 0;

// LDAP server address
$ldap_server = "";

// LDAP server port
$ldap_port = "";

// Base DN to be used when searching a user
$ldap_base_dn = "";

// Attribute that is used to search the username
$ldap_uid = "";

// Attribute that is used to search user's groups
$ldap_group = "";

// Name of the admin group
$ldap_admin_group = "";

// DN for making initial connection (leave blank for anonymous binding)
$ldap_user_dn = "";

// Password to be used against the userDN above (leave blank for anonymous binding)
$ldap_password = "";

// Full name attribute for the user
$ldap_fullname = "";

// Email ID attribute for the user
$ldap_mail = "";

// Avatar attribute for the user
$ldap_avatar = "";

// SMTP host for sending mail
$smtp_host = "localhost";

// Port used on the SMTP server for sending mail
$smtp_port = 25;

// SMTP server username (optional)
$smtp_username = "";

// SMTP server password (leave blank if no smtp_username spacified)
$smtp_password = "";

// From address for sending emails
$smtp_from = "webmaster@yoursite.com";

