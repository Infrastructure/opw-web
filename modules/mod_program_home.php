<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

// Collect some data
$id = $core->variable('prg', 0);

$program_data = $cache->get_program_data($id);

// Was the program found?
if ($program_data != null)
{
    $user->get_role($id, $role, $organization);

    // Set object availability based on deadlines
    $show_mentor = true;
    $show_started = false;

    $project_permissions = get_project_permissions($program_data, $role, null);

    $show_student = $project_permissions->can_submit;
    $late_submission = $project_permissions->late_submission;

    if ($core->timestamp >= $program_data['dl_mentor'])
    {
        // We still show the "View applications" button after dl_mentor
        // (the program announcement date) because we want to allow mentors
        // to sign up late in some cases.
        // $show_mentor = false;
        $show_started = true;
    }

    // Set deadlines placeholders
    $lang->assign(array(
        'dl_student'    => date('M d Y, h:i a T', $program_data['dl_student']),
        'dl_mentor'     => date('M d Y, h:i a T', $program_data['dl_mentor']),
    ));

    $show_resign = false;

    if ($role == 's')
    {
        if ($show_started) {
            $sql = "SELECT COUNT(*) AS count " .
                   "FROM {$db->prefix}participants p " .
                   "LEFT JOIN {$db->prefix}projects prj " .
                   "ON p.project_id = prj.id " .
                   "WHERE p.username = :username " .
                   "AND p.program_id = :program_id " .
                   "AND prj.is_accepted = 1 ";

            $row = $db->query($sql,
                              array('program_id' => $program_data['id'],
                                    'username' => $user->username),
                              true);

            if ($row['count'] > 0)
                $role_message = $lang->get('role_student_accepted');
            else
                $role_message = $lang->get('role_student_rejected');
        }
        else {
            $role_message = $lang->get('role_student_applied');
            $show_resign = true;
        }
    }
    else if ($role == 'm')
    {
        $role_message = $lang->get('role_mentor');
        $show_resign = !$show_started;
    }
    else if ($role == 'i')
    {
        $role_message = $lang->get('role_intermediate');
        $show_resign = true;
    }
    else if ($role == 'x')
    {
        $role_message = $lang->get('role_rejected');
    }
    else
    {
        $role_message = '';
    }

    if ($project_permissions->can_apply_mentor && $user->isMentorAnyProgram())
        $apply_mentor_message = $lang->get('will_mentor_again');
    else
        $apply_mentor_message = $lang->get('apply_mentor');

    // Assign screen data for the program
    $skin->assign(array(
        'program_id'               => $program_data['id'],
        'program_title'            => htmlspecialchars($program_data['title']),
        'program_description'      => nl2br($program_data['description']),
        'program_start_date'       => date('M d, Y', $program_data['start_time']),
        'program_end_date'         => date('M d, Y', $program_data['end_time']),
        'student_deadlines'        => $lang->get('student_dl_info'),
        'mentor_deadlines'         => $lang->get('mentor_dl_info'),
        'return_url'               => urlencode($core->request_uri()),
        'role_message'             => $role_message,
        'resign_visibility'        => $skin->visibility($show_resign),
        'prg_guest_visibility'     => $skin->visibility($role == 'g' || $role == 'r' || $role == 'x'),
        'prg_resign_visibility'    => $skin->visibility(false),
        'prg_rejected_visibility'  => $skin->visibility($role == 'x'),
        'prg_student_visibility'   => $skin->visibility($role == 's'),
        'prg_interm_visibility'    => $skin->visibility($role == 'i'),
        'prg_mentor_visibility'    => $skin->visibility($role == 'm'),
        'apply_student_visibility' => $skin->visibility($project_permissions->can_apply_student),
        'apply_mentor_visibility' => $skin->visibility($project_permissions->can_apply_mentor),
        'apply_mentor_message'     => $apply_mentor_message,
        'dl_student_visibility'    => $skin->visibility($show_student),
        'late_submission_visibility' => $skin->visibility($late_submission),
        'dl_mentor_visibility'     => $skin->visibility($show_mentor),
        'started_visibility'       => $skin->visibility($show_started),
        'prog_adm_visibility'      => $skin->visibility($user->is_admin),
        'modadm_visibility'        => $skin->visibility($user->is_admin),
    ));

    // Output the module
    $module_title = htmlspecialchars($program_data['title']);
    $module_data = $skin->output('tpl_program_home');
}
else
{
    $core->redirect("?q=view_programs");
}

?>
