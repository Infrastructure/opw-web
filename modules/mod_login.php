<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

// Collect some data
$username = $core->variable('username', '', false, true);
$redir_url = $core->variable('r', '');
$dest_url = !empty($redir_url) ? urldecode($redir_url) : $core->path();
$provider = $core->variable('p', '');

// Log the user out if already logged in
if ($user->is_logged_in)
{
    $core->redirect($dest_url);
}

// If we're on the end of successful authentication request
if ($provider != '' && $user->finish_login($provider))
    $core->redirect($dest_url);

// Login data was submitted
$login_openid = isset($_POST['login_openid']);
$login_google = isset($_POST['login_google']);
$login_facebook = isset($_POST['login_facebook']);

if ($login_openid) {
    if (!empty($username)) {
        // Check if user is banned
        $is_banned = $user->is_banned($username);

        // User isn't banned
        if (!$is_banned) {
            // Log in user
            $login_success = $user->login_openid($username);

            // Check if login succeeded
            if ($login_success) {
                $core->redirect($dest_url);
            }
            else {
                $error_message = $lang->get('login_error');
            }
        }
        else {
            $error_message = $lang->get('account_banned');
        }
    }
    else {
        $error_message = $lang->get('enter_openid');
    }
} else if ($login_google) {
   $login_success = $user->login_google($username);

   // Check if login succeeded
   if ($login_success) {
       $core->redirect($dest_url);
   }
   else {
       $error_message = $lang->get('login_error');
   }
} else if ($login_facebook) {
   $login_success = $user->login_facebook($username);

   // Check if login succeeded
   if ($login_success) {
       $core->redirect($dest_url);
   }
   else {
       $error_message = $lang->get('login_error');
   }
}

// Assign skin data
$skin->assign(array(
    'error_message'         => isset($error_message) ? $error_message : '',
    'error_visibility'      => $skin->visibility(isset($error_message)),
    'openid_visibility'     => $skin->visibility($config->auth_openid_enabled),
    'facebook_visibility'   => $skin->visibility($config->auth_facebook_enabled),
    'google_visibility'     => $skin->visibility($config->auth_google_enabled)
));

// Assign the module data and title
$module_title = $lang->get('log_in');
$module_data = $skin->output('tpl_login');

