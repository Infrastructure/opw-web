<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

// Get the username and return URL
$action = $core->variable('a', 'view');
$return_encoded = $core->variable('r', '');
$username_encoded = $core->variable('u', '');

$return_url = urldecode($return_encoded);
$username = urldecode($username_encoded);

// We need username for this module
if (empty($username)) {
   $user->restrict(!is_null($user->username));
   $username = $user->username;
   $username_encoded = urlencode($username);
}

$is_self = ($username == $user->username);
$can_edit = $is_self || $user->is_admin;
$can_view_details = $is_self || $user->is_admin || $user->isMentorAnyProgram();

// Get the user data
$username_data = $user->lookup_user($username);

$skin->assign(array(
   'visible_to_public' => $is_self ? $lang->get('visible_to_public') : '',
   'visible_to_mentors' => $is_self ? $lang->get('visible_to_mentors') : '',
   'visible_to_admins' => $is_self ? $lang->get('visible_to_admins') : ''
));

if ($action == 'view') {
    // Set the template data
    if (!is_null($username_data))
    {
        // And the identity data
        if ($can_view_details) {
            $sql = "SELECT * from {$db->prefix}identities " .
                "WHERE username = ?";
            $identity_data = $db->query($sql, $username);
        } else {
            $identity_data = array();
        }

        $identities_list = '';
        foreach ($identity_data as $row)
        {
            // Assign data for each project
            $skin->assign(array(
                    'identity_provider'      => $row['provider'],
                    'identity_full_name'     => htmlspecialchars($row['fullname']),
                    'identity_email'         => htmlspecialchars($row['email']),
                    'identity_email_verified' => $row['emailVerified'] != 0 ?
                                                  $lang->get('verified') : $lang->get('unverified'),
                    'identity_profile_url'   => htmlspecialchars($row['profileUrl'])
                ));

                $identities_list .= $skin->output('tpl_user_profile_identity');
        }

        $sql = "SELECT prj.*, org.title as org_title from {$db->prefix}projects prj " .
               "LEFT JOIN {$db->prefix}participants prt " .
                   "ON prt.project_id = prj.id " .
               "LEFT JOIN {$db->prefix}organizations org " .
                   "ON prj.organization_id = org.id " .
               "WHERE prt.username = ? AND prt.role = 's'" .
               "ORDER BY prj.program_id DESC, prt.project_id";

        $project_data = $db->query($sql, $username);
        $program_id = null;

        $projects_list = "";
        $projects_visibility = false;

        foreach ($project_data as $row)
        {
            if ($row['program_id'] != $program_id) {
                $program_id = $row['program_id'];
                $user->get_role($program_id, $role, $mentor_organization_id);
                $program_data = $cache->get_program_data($program_id);
                $shown_header = false;
            }

            // This will do another DB query to see if the user owns the project; accept
            // that to keep permissions logic unified.
            $project_permissions = get_project_permissions($program_data, $role, $row);
            if (!$project_permissions->can_view)
                continue;

            if (!$shown_header) {
                $shown_header = true;
                $projects_visibility = true;

                $skin->assign(array(
                    'program_title'      => $program_data['title'],
                ));

                $projects_list .= $skin->output('tpl_user_profile_project_header');
            }

            if ($role == 'm' || $user->is_admin) {
                $org_opinion = $lang->get('opinion_' . $row['org_opinion']);
                $organization = "<a href='?q=manage_projects&amp;prg={$program_id}&amp;o={$row['organization_id']}'>" . htmlspecialchars($row['org_title']) . "</a>";
            } else {
                $org_opinion = '';
                $organization = htmlspecialchars($row['org_title']);
            }

            $skin->assign(array(
                              'project_url'    => "?q=view_projects&amp;prg={$program_id}&amp;p={$row['id']}",
                              'project_title'  => htmlspecialchars($row['title']),
                              'badges'         => build_project_badges($role, $row, 0, 0),
                              'organization'   => $organization,
                              'opinion'        => $org_opinion,
                              ));

            $projects_list .= $skin->output('tpl_user_profile_project');
        }

        $contracts_list = "";
        $contracts_visibility = false;

        // The old way of doing contracts was that the user uploaded an attachment, and the
        // administrator approved it; in the new way of doing things, the user simply
        // accepts the contract themself with a click-through. We still display contracts
        // that were previously uploaded, and show the admin interface for approving
        // contracts if any contracts were uploaded, but there is no longer any interface
        // for uploading new contracts, and instead the user is given a link to the
        // interface for the click-through agreement.
        if ($is_self || $user->is_admin) {

            // Find all programs where the user is a student or mentor for an
            // accepted project.
            $sql = "SELECT prg.*, r.role, r.contract_approved, r.contract_accepted_time, r.contract_entered_name FROM {$db->prefix}roles r " .
                   "LEFT JOIN {$db->prefix}programs prg " .
                   "ON prg.id = r.program_id " .
                   "WHERE r.username = ? " .
                   "AND prg.dl_mentor < UNIX_TIMESTAMP() " .
                   "AND EXISTS (SELECT * from {$db->prefix}participants prt " .
                               "LEFT JOIN {$db->prefix}projects prj " .
                               "ON prt.project_id = prj.id " .
                               "WHERE prt.program_id = prg.id AND prt.username = r.username AND prj.is_accepted = 1) " .
                   "ORDER BY prg.id DESC";
            $program_data = $db->query($sql, $username);

            $contracts_by_program = array();
            foreach ($program_data as $row)
            {
                $contracts_by_program[$row['id']] = array();
            }

            // See if there are any uploaded contracts
            $sql = "SELECT id, program_id, name, description FROM {$db->prefix}attachments " .
                   "WHERE uploader = ? AND is_contract = 1 ";
            $contract_data = $db->query($sql, $username);

            foreach ($contract_data as $row)
            {
                array_push($contracts_by_program[$row['program_id']], $row);
            }

            // Now iterate through all programs where the user is a mentor or student
            foreach ($program_data as $program)
            {
                $contract_return = urlencode($core->request_uri());

                $contracts_visibility = true;
                $skin->assign(array(
                                  'program_title' => $program['title'],
                                  'role' => $lang->get('role_' . $program['role']),
                                  'approved_visibility' => $skin->visibility($program['contract_approved']),
                                  'accepted_visibility' => $skin->visibility($program['contract_accepted_time'] != 0),
                                  'not_accepted_visibility' => $skin->visibility(!$is_self && !$program['contract_approved'] && $program['contract_accepted_time'] == 0),
                                  'contract_accepted_time' => date('M d, Y H:i:s', $program['contract_accepted_time']) . ' UTC',
                                  'contract_entered_name' => htmlspecialchars($program['contract_entered_name']),
                                  'accepted' => $skin->visibility($program['contract_approved']),
                             ));

                $contracts_list .= $skin->output('tpl_user_profile_contract_header');

                foreach ($contracts_by_program[$program['id']] as $contract) {
                    $view_url = "?q=attachment&prg={$program['id']}&i={$contract['id']}";
                    $delete_url = "?q=attachment&a=delete&prg={$program['id']}&i={$contract['id']}&r={$contract_return}";
                    $skin->assign(array(
                                      'attachment_id' => htmlspecialchars($contract['id']),
                                      'name' => htmlspecialchars($contract['name']),
                                      'view_url' => htmlspecialchars($view_url),
                                      'delete_url' => htmlspecialchars($delete_url),
                                      'description' => htmlspecialchars($contract['description']),
                                      'delete_visibility' => $skin->visibility($program['contract_approved'] == 0 || $user->is_admin)
                                 ));
                    $contracts_list .= $skin->output('tpl_user_profile_contract');
                }

                if ($is_self) {
                    $review_contract_visibility = false;
                    $view_contract_visibility = false;

                    if ($is_self && $program['contract_accepted_time'] == 0) {
                        $review_contract_visibility = $program['is_active'] && $program['contracts_active'];
                    } else if ($program['contract_accepted_time'] != 0) {
                        $view_contract_visibility = true;
                    }
                    if ($review_contract_visibility || $view_contract_visibility) {
                        $contract_url = "?q=contract&prg={$program['id']}&r={$contract_return}";
                        $skin->assign(array(
                                          'contract_url' => htmlspecialchars($contract_url),
                                          'review_contract_visibility' => $skin->visibility($review_contract_visibility),
                                          'view_contract_visibility' => $skin->visibility($view_contract_visibility),
                                          ));
                        $contracts_list .= $skin->output('tpl_user_profile_contract_footer');
                    }
                }

                if ($user->is_admin && count($contracts_by_program[$program['id']]) > 0) {
                    $approve_url = "?q=attachment&a=approve_contract&prg={$program['id']}&u={$username}&r={$contract_return}";
                    $unapprove_url = "?q=attachment&a=unapprove_contract&prg={$program['id']}&u={$username}&r={$contract_return}";
                    $skin->assign(array(
                                      'program_id' => $program['id'],
                                      'approve_url' => htmlspecialchars($approve_url),
                                      'unapprove_url' => htmlspecialchars($unapprove_url),
                                      'approve_visibility' => $skin->visibility($program['contract_approved'] == 0),
                                      'unapprove_visibility' => $skin->visibility($program['contract_approved'] == 1)
                                  ));
                    $contracts_list .= $skin->output('tpl_user_profile_contract_admin_footer');
                }
            }
        }

        $is_admin   = false;
        $avatar_url = "?q=user_avatar&amp;u={$username_encoded}";
        $edit_url = "?q=user_profile&amp;a=editor&amp;u={$username_encoded}";
        $full_name  = $username_data['fullname'];
        $user_email = $can_view_details ? $username_data['email'] : '';
        $user_email_verified = $can_view_details ? $username_data['emailVerified'] : false;
        $website_url = $username_data['websiteUrl'];
        $is_admin = $username_data['is_admin'];

        // Assign profile variables
        $skin->assign(array(
            'user_username'         => htmlspecialchars($username),
            'user_fullname'         => htmlspecialchars($full_name),
            'user_email'            => htmlspecialchars($user_email),
            'user_email_verified'   => $user_email_verified ? $lang->get('{{verified}}') : $lang->get('{{unverified}}'),
            'website_url'           => htmlspecialchars($website_url),
            'avatar_url'            => $avatar_url,
            'return_url'            => $return_url,
            'edit_url'              => $edit_url,
            'identities_list'       => $identities_list,
            'projects_list'         => $projects_list,
            'contracts_list'        => $contracts_list,
            'profile_visibility'    => $skin->visibility(true),
            'notice_visibility'     => $skin->visibility(false),
            'edit_visibility'       => $skin->visibility($can_edit),
            'details_visibility'    => $skin->visibility($can_view_details),
            'projects_visibility'   => $skin->visibility($projects_visibility),
            'contracts_visibility'  => $skin->visibility($contracts_visibility),
            'contact_visibility'    => $skin->visibility(!$is_self && $can_view_details),
            'badge_visibility'      => $skin->visibility($is_admin),
            'return_visibility'     => $skin->visibility(empty($return_url), true),
        ));    
    }
    else
    {
        // No profile found, show notice
        $skin->assign(array(
            'profile_visibility'    => $skin->visibility(false),
            'notice_visibility'     => $skin->visibility(true),
        ));
    }
    // Output the page
    $module_title = $lang->get('user_profile');
    $module_data = $skin->output('tpl_user_profile');

} else if ($action == 'editor') {
    $user->restrict($can_edit);

    if (isset($_POST['user_save'])) {
        $user->check_csrf();

        $fullname = $core->variable('fullname', '', false, true);
        $email = $core->variable('email', '', false, true);
        $website = $core->variable('website', '', false, true);

        if ($email == $username_data['email']) {
            $email_verified = $username_data['emailVerified'];
        } else {
            // Check if the user is changing the email back to a verified login email
            $sql = "SELECT COUNT(*) as count from {$db->prefix}identities " .
                   "WHERE username = :username " .
                       "AND email = :email " .
                       "AND emailVerified = 1";
            $row = $db->query($sql,
                              array('username' => $username,
                                    'email' => $email),
                              true);
            $email_verified = $row['count'] > 0;
        }

        $sql = "UPDATE {$db->prefix}profiles " .
               "SET fullname = :fullname, " .
                   "email = :email, " .
                   "emailVerified = :email_verified, " .
                   "websiteUrl = :website_url " .
               "WHERE username = :username";
        $db->query($sql,
                   array('username' => $username,
                         'fullname' => $fullname,
                         'email' => $email,
                         'email_verified' => $email_verified ? 1 : 0,
                         'website_url' => $website));

        $core->redirect("?q=user_profile&u={$username_encoded}");
    }

    // Assign profile variables
    $skin->assign(array(
        'user_fullname'         => htmlspecialchars($username_data['fullname']),
        'user_email'            => htmlspecialchars($username_data['email']),
        'website_url'           => htmlspecialchars($username_data['websiteUrl']),
        'cancel_url'            => "?q=user_profile&amp;u={$username_encoded}",
        'error_visibility'      => $skin->visibility(false),
        'error_message'         => ''
    ));

    // Output the page
    $module_title = $lang->get('edit_user_profile');
    $module_data = $skin->output('tpl_user_profile_editor');
}
else
{
    // Unknown action
    $core->redirect($core->path());
}

?>