<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

$program_id = $core->variable('prg', 0);
$accepted = $core->variable('accepted', 0) != 0;

$program_data = $cache->get_program_data($program_id);
$user->restrict($program_data !== null);

// Get the program's participant list

if ($accepted) {
    $accepted_clause = "AND pr.id IS NOT NULL " .
                       "AND pr.is_accepted = 1 ";
} else {
    $accepted_clause = '';
}

$sql = "SELECT r.username, r.role AS role, r.contract_approved, r.contract_accepted_time, r.contract_entered_name, " .
       "pr.id AS project_id, pr.title AS project_title, pr.is_accepted, " .
       "pf.fullname, " .
       "(SELECT COUNT(*) FROM {$db->prefix}attachments a " .
            "WHERE a.uploader = r.username AND a.is_contract = 1 AND a.program_id = {$program_id}) " .
            " AS contract_count " .
       "FROM {$db->prefix}roles r " .
       "LEFT JOIN {$db->prefix}profiles pf " .
       "ON r.username = pf.username ".
       "LEFT JOIN {$db->prefix}participants p " .
       "ON r.program_id = p.program_id " .
       "AND r.username = p.username " .
       "LEFT JOIN {$db->prefix}projects pr " .
       "ON p.project_id = pr.id " .
       "WHERE r.program_id = {$program_id} " .
       $accepted_clause .
       "ORDER BY r.role, r.username";
$list_data = $db->query($sql);

// Parse the participant list
$list = array();
$prev_row = null;
$project = null;

foreach ($list_data as $row)
{
    // Append project to previous row
    if ($prev_row != null)
    {
        if ($prev_row['username'] == $row['username'] && $prev_row['role'] == $row['role'] &&
            $row['project_id'] != null)
        {
            $idx = count($list) - 1;
            $title = ($row['is_accepted'] == 1 ? '* ' : '') . $row['project_title'];
            if ($row['is_accepted'] == 1)
                $list[$idx]['any_accepted'] = true;
            $list[$idx]['projects'] .= '<br /><a href="?q=view_projects&prg=' . $program_id .
                                       '&p=' . $row['project_id'] . '">' . htmlspecialchars($title) .
                                       '</a>';
            continue;
        }
    }

    // Link to project only if it exists
    if ($row['project_id'] != null)
    {
        $project = '<a href="?q=view_projects&prg=' . $program_id . '&p=' . $row['project_id'] . '">' .
                   htmlspecialchars($row['project_title']) . '</a>';
    }
    else
    {
        $project = '-';
    }

    $list[] = array(
        'username'  => $row['username'],
        'profile'   => $user->profile($row['username'], true, $row['fullname']),
        'role'      => $row['role'],
        'projects'  => $project,
        'any_accepted' => $row['is_accepted'] == 1,
        'contract_approved'  => $row['contract_approved'],
        'contract_accepted_time'  => $row['contract_accepted_time'],
        'contract_entered_name'  => $row['contract_entered_name'],
        'contract_count'     => $row['contract_count']
    );

    $prev_row = $row;
}

// Populate the parsed list
$participant_list = '';

foreach ($list as $item)
{
    if ($item['contract_approved'] == 1)
        $contract_approved = $lang->get('yes');
    else if ($item['contract_accepted_time'] != 0)
        $contract_approved = $lang->get('yes') . " (" . htmlspecialchars($item['contract_entered_name']) . ")";
    else if ($item['any_accepted'] == 1) {
        if ($item['contract_count'] > 0)
            $contract_approved = $lang->get('uploaded');
        else
            $contract_approved = $lang->get('no');
    } else
        $contract_approved = '';

    // Assign data for each mentor
    $skin->assign(array(
        'participant'    => $item['profile'],
        'role'           => get_role_string($item['role'], $item['any_accepted']),
        'contract_approved' => $contract_approved,
        'projects'       => $item['projects'],
    ));

    $participant_list .= $skin->output('tpl_view_participants_item');
}

if ($accepted)
    $module_title = $lang->get('accepted_participants');
else
    $module_title = $lang->get('prog_participants');

// Assign final skin data
$skin->assign(array(
    'title'                 => $module_title,
    'participant_list'      => $participant_list,
    'notice_visibility'     => $skin->visibility(count($list) == 0),
    'list_visibility'       => $skin->visibility(count($list) > 0),
    'archive_url'           => "?q=contract&amp;a=archive&amp;prg={$program_id}",
));

// Output the module
$module_data = $skin->output('tpl_view_participants');

?>
