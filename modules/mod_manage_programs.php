<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

// Collect some data
$action = $core->variable('a', 'list');
$id = $core->variable('prg', 0);
$title = $core->variable('title', '', false, true);
$description = $core->variable('description', '', false, true);
$application_prefill = $core->variable('application_prefill', '', false, true);
$start_date = $core->variable('start_date', '', false, true);
$end_date = $core->variable('end_date', '', false, true);
$dl_student_date = $core->variable('dl_student', '', false, true);
$dl_mentor_date = $core->variable('dl_mentor', '', false, true);
$active = $core->variable('active', '') == "on" ? 1 : 0;
$contracts_active = $core->variable('contracts_active', '') == "on" ? 1 : 0;
$page = $core->variable('pg', 1);
$limit_start = ($page - 1) * $config->per_page;

$program_save = isset($_POST['program_save']);
$reject_undecided = isset($_POST['reject_undecided']);
$confirm = isset($_POST['yes']);

// Serve the page based on the action
if ($action == 'list')
{
    $programs_list = '';

    // Get all programs
    $sql = "SELECT * FROM {$db->prefix}programs " .
           "LIMIT :start, :count";
    $program_data = $db->query($sql, array('i:start' => $limit_start,
                                           'i:count' => $config->per_page));

    // Get program count
    $sql = "SELECT COUNT(*) AS count FROM {$db->prefix}programs";
    $program_count = $db->query($sql, null, true);

    // Build the list
    foreach ($program_data as $row)
    {
        // Assign data for this program
        $skin->assign(array(
            'prog_id'          =>    $row['id'], # 'program_id' is always the current program
            'program_title'       => htmlspecialchars($row['title']),
            'program_description' => nl2br(htmlspecialchars($row['description'])),
            'program_active'      => $skin->visibility($row['is_active'] == 1),
            'program_inactive'    => $skin->visibility($row['is_active'] == 0),
        ));

        $programs_list .= $skin->output('tpl_manage_programs_item');
    }

    // Get the pagination
    $pagination = $skin->pagination($program_count['count'], $page);

    // Assign final skin data
    $skin->assign(array(
        'programs_list'     => $programs_list,
        'list_pages'        => $pagination,
        'notice_visibility' => $skin->visibility(count($program_data) == 0),
        'list_visibility'   => $skin->visibility(count($program_data) > 0),
        'pages_visibility'  => $skin->visibility($program_count['count'] > $config->per_page),
    ));

    // Output the module
    $module_title = $lang->get('manage_programs');
    $module_data = $skin->output('tpl_manage_programs');
}
else if ($action == 'editor' && $reject_undecided)
{
  $user->check_csrf();
  $sql = "UPDATE {$db->prefix}projects SET is_accepted = 0 " .
         "WHERE is_accepted = -1 " .
         "  AND program_id = :id";
  $db->query($sql, array('id' => $id));
  $core->redirect("?q=manage_programs&a=editor&prg={$id}");
}
else if ($action == 'editor')
{
    $page_title = $id == 0 ? $lang->get('add_program') : $lang->get('edit_program');
    $start_time = strtotime($start_date);
    $end_time = strtotime($end_date);
    $dl_student = strtotime($dl_student_date);
    $dl_mentor = strtotime($dl_mentor_date);

    if ($program_save)
    {
        $user->check_csrf();

        if (empty($title) || empty($start_date) || empty($end_date) ||
            empty($dl_student_date) || empty($dl_mentor_date))
        {
            $error_message = $lang->get('err_mandatory_fields');
        }
        else if ($start_time === false || $end_time === false)
        {
            $error_message = $lang->get('invalid_date');
        }
        else if ($dl_student === false || $dl_mentor === false)
        {
            $error_message = $lang->get('invalid_deadlines');
        }
        else
        {
            // Determine deadline and completion flags
            $deadline = $dl_student < $core->timestamp ? 1 : 0;
            $complete = $end_time < $core->timestamp ? 1 : 0;

            $params = array('id' => $id,
                            'title' => $title,
                            'description' => $description,
                            'application_prefill' => $application_prefill,
                            'start_time' => $start_time,
                            'end_time' => $end_time,
                            'dl_student' => $dl_student,
                            'dl_mentor' => $dl_mentor,
                            'active' => $active,
                            'contracts_active' => $contracts_active,
                            'deadline' => $deadline,
                            'complete' => $complete);

            if ($id > 0)
            {
                // Update program data
                $sql = "UPDATE {$db->prefix}programs " .
                       "SET title = :title, " .
                       "    description = :description, " .
                       "    application_prefill = :application_prefill, " .
                       "    start_time = :start_time, " .
                       "    end_time = :end_time, " .
                       "    dl_student = :dl_student, " .
                       "    dl_mentor = :dl_mentor, " .
                       "    is_active = :active, " .
                       "    contracts_active = :contracts_active " .
                       "WHERE id = :id";
                $db->query($sql, $params);

                // Update program flags in the queue
                // Usually, one entry for the program is exptected to
                // be there, unless the program is updated post completion,
                // which is highly unlikely.
                $sql = "UPDATE {$db->prefix}queue " .
                       "SET deadline = :deadline, " .
                       "    complete = :complete " .
                       "WHERE program_id = :id";
                $db->query($sql, $params);
            }
            else
            {
                // Insert program data
                $sql = "INSERT INTO {$db->prefix}programs " .
                       "(title, description, application_prefill, start_time, end_time, " .
                       " dl_student, dl_mentor, is_active) " .
                       "VALUES (:title, :description, :application_prefill, :start_time, " .
                       "        :end_time, :dl_student, :dl_mentor, :active)";
                $db->query($sql, $params);

                // Get the new program ID
                $params['id'] = $db->get_id();

                // Insert new entry to the queue
                $sql = "INSERT INTO {$db->prefix}queue " .
                       "(program_id, deadline, complete) " .
                       "VALUES (:id, :deadline, :complete)";
                $db->query($sql, $params);
            }

            // Purge the programs cache
            $cache->purge('programs');

            // Redirect to list page
            $core->redirect("?q=manage_programs");
        }
    }

    // Load data when in edit mode
    if ($id > 0)
    {
        $sql = "SELECT * FROM {$db->prefix}programs " .
               "WHERE id = ?";
        $row = $db->query($sql, array($id), true);

        // Set loaded data
        $title = $row['title'];
        $description = $row['description'];
        $application_prefill = $row['application_prefill'];
        $start_date = date('M d Y, h:i a', $row['start_time']);
        $end_date = date('M d Y, h:i a', $row['end_time']);
        $dl_student = date('M d Y, h:i a', $row['dl_student']);
        $dl_mentor = date('M d Y, h:i a', $row['dl_mentor']);
        $active = $row['is_active'];
        $contracts_active = $row['contracts_active'];
    }

    // Assign skin data
    $skin->assign(array(
        'editor_title'      => $page_title,
        'title'             => htmlspecialchars($title),
        'description'       => escapenewlines(htmlspecialchars($description)),
        'application_prefill' => escapenewlines(htmlspecialchars($application_prefill)),
        'start_date'        => $start_date,
        'end_date'          => $end_date,
        'dl_student'        => $dl_student,
        'dl_mentor'         => $dl_mentor,
        'student_contract_edit' => "?q=contract&amp;a=edit_student&amp;prg={$id}",
        'student_contract_sample' => "?q=contract&amp;a=sample_student&amp;prg={$id}",
        'mentor_contract_edit' => "?q=contract&amp;a=edit_mentor&amp;prg={$id}",
        'mentor_contract_sample' => "?q=contract&amp;a=sample_mentor&amp;prg={$id}",
        'active_checked'    => $skin->checked($active == 1),
        'contracts_active_checked' => $skin->checked($contracts_active == 1),
        'error_message'     => isset($error_message) ? $error_message : '',
        'error_visibility'  => $skin->visibility(isset($error_message)),
        'delete_visibility' => $skin->visibility($id > 0),
        'delete_url'        => "?q=manage_programs&amp;a=delete&amp;prg={$id}",
    ));

    // Output the module
    $module_title = $page_title;
    $module_data = $skin->output('tpl_manage_programs_editor');
}
else if ($action == 'delete')
{
    // Deletion was confirmed
    if ($confirm)
    {
        $user->check_csrf();

        $params = array('id' => $id);

        $sql = "DELETE FROM {$db->prefix}roles " .
               "WHERE program_id = :id";
        $db->query($sql, $params);

        $sql = "DELETE FROM {$db->prefix}participants " .
               "WHERE program_id = :id";
        $db->query($sql, $params);

        $sql = "DELETE FROM {$db->prefix}projects " .
               "WHERE program_id = :id";
        $db->query($sql, $params);

        $sql = "DELETE FROM {$db->prefix}queue " .
               "WHERE program_id = :id";
        $db->query($sql, $params);

        $sql = "DELETE FROM {$db->prefix}programs " .
               "WHERE id = :id";
        $db->query($sql, $params);

        // Purge the cache data
        $cache->purge(array('programs', 'projects', 'roles'));

        // Redirect to list page
        $core->redirect("?q=manage_programs");
    }

    // Assign confirm box data
    $skin->assign(array(
        'message_title'     => $lang->get('confirm_deletion'),
        'message_body'      => $lang->get('confirm_program_del'),
        'cancel_url'        => "?q=manage_programs&amp;a=editor&amp;prg={$id}",
    ));

    // Output the module
    $module_title = $lang->get('confirm_deletion');
    $module_data = $skin->output('tpl_confirm_box');
}

?>
