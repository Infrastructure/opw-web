<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

require_once 'Michelf/Markdown.inc.php';

$program_id = $core->variable('prg', 0);
$return_url = $core->variable('r', '');
$action = $core->variable('a', '')
;
$program_data = $cache->get_program_data($program_id);

$user->restrict($program_data != null);

if ($action == 'edit_student' || $action == 'edit_mentor') {
    $user->restrict($user->is_admin);

    $is_student = $action == 'edit_student';
    $is_mentor = $action == 'edit_mentor';
    $markdown = $core->variable('markdown', '');

    if ($markdown != '') {
        $is_preview = true;
    } else {
        $is_preview = false;

        $sql = "SELECT contents from {$db->prefix}contracts " .
                "WHERE program_id = :program_id AND role = :role " .
                "ORDER BY id DESC";

        $row = $db->query($sql,
                          array('program_id' => $program_id,
                                'role' => $is_student ? 's' : 'm'),
                          true);
        $markdown = $row['contents'];
    }

    $save_contract = isset($_POST['save_contract']);

    if ($save_contract) {
        $user->check_csrf();

        $is_preview = false;

        $sql = "INSERT INTO {$db->prefix}contracts (program_id, role, uploader, upload_time, contents) " .
               "VALUES (:program_id, :role, :username, UNIX_TIMESTAMP(), :contents)";

        $db->query($sql,
                   array('program_id' => $program_id,
                         'role' => $is_student ? 's' : 'm',
                         'username' => $user->username,
                         'contents' => $markdown)
                  );

        if ($return_url != '')
            $core->redirect($return_url);
    }

    $html = Michelf\Markdown::defaultTransform($markdown);

    $skin->assign(array(
         'program_mentor_visibility' => $skin->visibility($is_mentor),
         'program_student_visibility' => $skin->visibility($is_student),
         'preview_visibility' => $skin->visibility($is_preview),
         'title'      => $program_data['title'],
         'markdown'   => escapenewlines($markdown),
         'html'       => $html,
                 ));

    // Output the module
    $module_title = $lang->get('edit_contract');
    $module_data = $skin->output('tpl_edit_contract');
} else if ($action == 'archive') {
    // Get all the contracts for a program as a zip-file archive of PDF files

    $user->restrict($user->is_admin);

    // Generating PDF files can take a long time
    set_time_limit(120); // Two minutes

    // We require this after checking for admin status to reduce any security exposure
    require_once('tcpdf/tcpdf.php');

    // Find all the participants for this program that have signed contracts
    $sql = "SELECT r.*, pf.fullname, pf.email, " .
           "EXISTS (SELECT * from {$db->prefix}participants prt " .
               "LEFT JOIN {$db->prefix}projects prj " .
               "ON prj.id = prt.project_id " .
               "WHERE prt.program_id = r.program_id AND prt.username = r.username AND prj.is_accepted = 1) as has_project " .
           "FROM {$db->prefix}roles r " .
           "LEFT JOIN {$db->prefix}profiles pf on pf.username = r.username " .
           "WHERE contract_accepted_time != 0 AND program_id = :program_id";

    $role_data = $db->query($sql,
                            array('program_id' => $program_id));

    // Avoid looking up the same contract multiple times from the database
    $contracts_by_id = array();

    $zipfile = new ZipArchive;
    $zipname = tempnam("/tmp", "contracts_archive");
    $zipfile->open($zipname, ZipArchive::CREATE);

    $folder_name = preg_replace("/\W+/", "_", $program_data["title"]);

    foreach ($role_data as $row)
    {
        $contract_id = $row['contract_id'];
        if (!array_key_exists($contract_id, $contracts_by_id)) {
            $sql = "select contents from {$db->prefix}contracts " .
                   "WHERE id = ?";
            $contract_row = $db->query($sql, $row['contract_id'], true);
            $markdown = $contract_row['contents'];;
            $contract_htmlhtml = Michelf\Markdown::defaultTransform($markdown);
            $contracts_by_id[$contract_id] = Michelf\Markdown::defaultTransform($markdown);
        }

        $pdf = new TCPDF('P', 'in', 'USLETTER',
                         true /*unicode */, 'UTF-8', false /* !PDF/A */);
        $pdf->SetMargins(0.5, 0.5, 0.5); /* left, top, right, margin */
        $pdf->setFooterMargin(0.5);
        $pdf->setHeaderMargin(0.5);
        $pdf->SetAutoPageBreak(TRUE, 0.75 /* bottom margin */);
        $pdf->setFontSubsetting(true);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

        $pdf->AddPage();

        $pdf->SetFontSize(13);
        $pdf->SetFont('helvetica', 'B');

        $pdf->write(0, $program_data["title"] . "\n");

        $pdf->SetFontSize(11);
        $pdf->SetFont('helvetica', '');
        $pdf->write(0, "Full name: " . $row["fullname"] . "\n");
        $pdf->write(0, "Username: " . $row["username"] . "\n");

        $role_string = get_role_string($row['role'], $row['has_project']);

        $pdf->write(0, "Role: " . $role_string . "\n");
        $pdf->write(0, "Email: " . $row["email"] . "\n");
        $pdf->write(0, "\n");

        // Show the contract text in a shaded box at a smaller size
        $html = Michelf\Markdown::defaultTransform($contracts_by_id[$contract_id]);
        $pdf->SetFontSize(9);
        $pdf->SetFillColor(255, 250, 240);
        $pdf->SetDrawColor(128, 125, 120);
        $pdf->writeHTMLCell(0, 0, '', '',
                            $html,
                            1 /*border */, 1 /* position on next line afterwards */, true /* fill */);

        // Now show the signature information
        $pdf->SetFontSize(11);
        $pdf->write(0, "\n");
        $contract = 'E-Signed';
        $contract .= ' by "' . $row['contract_entered_name'] . '"';
        $contract .= ' on ' . date('M d, Y H:i:s', $row['contract_accepted_time']) . ' UTC';
        if ($row['contract_accepted_from'] != '')
            $contract .= ' from ' . str_replace(' ', ' via ', $row['contract_accepted_from']);
        $contract .= ' using ' .$config->site_url;

        $pdf->write(0, $contract);

        // Insert into the archive

        // Software Freedom Conservancy wants contract files named in a very specific fashion
        // YYYY-MM-DD_FULL-NAME_Outreachy-2015-05_TYPE-agreement.pdf
        $file_name = date('Y-m-d', $row['contract_accepted_time']);
        $file_name .= '_';
        $file_name .= preg_replace('/[^a-zA-Z]+/', '-', $row['contract_entered_name']);
        $file_name .= '_Outreachy-';
        $file_name .= date('Y-m', $program_data['dl_mentor']);
        $file_name .= '-';
        $file_name .= strtolower($role_string);
        $file_name .= '-agreement.pdf';

        $zipfile->addFromString($folder_name . "/" . $file_name, $pdf->Output('', 'S'));
    }

    $zipfile->close();

    header("Content-Type: application/zip");
    header("Content-Disposition: filename=\"{$folder_name}.zip\"");
    header('Content-Length: ' . filesize($zipname));
    readfile($zipname);

    unlink($zipname);

    exit;
} else {
    $accept_contract = isset($_POST['accept_contract']);

    if ($action == 'sample_student' || $action == 'sample_mentor') {
        $is_sample = true;
        $is_student = $action == 'sample_student';
        $is_mentor = $action == 'sample_mentor';
        $contract_accepted_time =  0;
        $contract_accepted_from = '';
        $contract_entered_name = '';
        $contract_id = NULL;
    } else {        $is_sample = false;
        $user->restrict(!is_null($user->username));
        $username = $user->username;

        $sql = "SELECT r.*, " .
               "EXISTS (SELECT * from {$db->prefix}participants prt " .
                       "LEFT JOIN {$db->prefix}projects prj " .
                              "ON prj.id = prt.project_id " .
                       "WHERE prt.program_id = r.program_id AND prt.username = r.username AND prj.is_accepted = 1) as has_project " .
               "FROM {$db->prefix}roles r " .
                   "WHERE r.username = :username " .
                   "AND r.program_id = :program_id";

        $role_data = $db->query($sql,
                                array('username' => $username,
                                      'program_id' => $program_id),
                                true);


        $user->restrict($role_data['has_project']);

        $contract_accepted_time =  $role_data['contract_accepted_time'];
        $contract_accepted_from = $role_data['contract_accepted_from'];
        $contract_entered_name =  $role_data['contract_entered_name'];

        $user->restrict($contract_accepted_time != 0 ||
                        ($program_data['is_active'] &&
                         $program_data['contracts_active'] &&
                         $core->timestamp > $program_data['dl_mentor']));

        $is_student = $role_data['role'] == 's';
        $is_mentor = $role_data['role'] == 'm';
    }

    $has_accepted = $contract_accepted_time != 0;

    if ($has_accepted) {
        $contract_id = $role_data['contract_id'];

        $sql = "SELECT contents from {$db->prefix}contracts " .
               "WHERE id = :contract_id";

        $row = $db->query($sql,
                          array('contract_id' => $contract_id),
                          true);
        $contract_contents = $row['contents'];
    } else {
        $sql = "SELECT id, contents from {$db->prefix}contracts " .
                "WHERE program_id = :program_id AND role = :role " .
                "ORDER BY id DESC";

        $row = $db->query($sql,
                          array('program_id' => $program_id,
                                'role' => $is_student ? 's' : 'm'),
                          true);
        $contract_id = $row['id'];
        $contract_contents = $row['contents'];
    }

    if ($accept_contract) {
        $user->check_csrf();

        $contract_name = trim($core->variable('contract_name', ''));
        $user->restrict($contract_name != '');

        $contract_accepted_from = $core->get_remote_addrs();

        // The most remote addresses are spoofable, so the most reliable
        // part of the remote addresses is the *end* part, so truncate at
        // te beginning.
        if (strlen($contract_accepted_from) > 255)
          $contract_accepted_from = "... " + substr($contract_accepted_from, -251);

        if (!$is_sample) {
            $sql = "UPDATE {$db->prefix}roles " .
                      "SET contract_accepted_time = UNIX_TIMESTAMP(), " .
                      "    contract_id = :contract_id, " .
                      "    contract_entered_name = :contract_name, " .
                      "    contract_accepted_from = :contract_accepted_from " .
                    "WHERE username = :username " .
                      "AND program_id = :program_id";

            $db->query($sql,
                       array('username' => $username,
                             'program_id' => $program_id,
                             'contract_id' => $contract_id,
                             'contract_name' => $contract_name,
                             'contract_accepted_from' => $contract_accepted_from));
        }

        if ($return_url != '')
            $core->redirect($return_url);
    }

    $contract_html = Michelf\Markdown::defaultTransform($contract_contents);
    $via = ' ' . $lang->get('via') . ' ';

    $skin->assign(array(
         'view_visibility'   => $skin->visibility($has_accepted),
         'review_visibility' => $skin->visibility(!$has_accepted),
         'from_visibility' => $skin->visibility($contract_accepted_from != ''),
         'program_mentor_visibility' => $skin->visibility($is_mentor),
         'program_student_visibility' => $skin->visibility($is_student),
         'contract_accepted_time' => date('M d, Y H:i:s', $contract_accepted_time) . ' UTC',
         'contract_entered_name' => htmlspecialchars($contract_entered_name),
         'contract_accepted_from' => htmlspecialchars(str_replace(' ', $via, $contract_accepted_from)),
         'contract_html' => $contract_html,
                 ));

    // Output the module
    $module_title = $lang->get($has_accepted ? 'view_contract' : 'review_contract');
    $module_data = $skin->output('tpl_contract');
}

?>
