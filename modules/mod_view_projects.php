<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

// Collect some data
$action = $core->variable('a', 'view');
$category = $core->variable('c', '');
$program_id = 0 + $core->variable('prg', 0);
$project_id = 0 + $core->variable('p', 0);
$organization_id = 0 + $core->variable('o', 0);
$title = $core->variable('title', '', false, true);
$description = $core->variable('description', '', false, true);
$new_student = $core->variable('new_student', '', false, true);
$new_mentor = $core->variable('new_mentor', '', false, true);
$return_url = $core->variable('r', '');
$late_edit = $core->variable('late_edit', '') == 'on' ? 1 : 0;
$page = $core->variable('pg', 1);
$limit_start = ($page - 1) * $config->per_page;

$mentor_apply = isset($_POST['mentor_apply']);
$mentor_remove = isset($_POST['mentor_remove']);
$project_save = isset($_POST['project_save']);
$confirm = isset($_POST['yes']);

// Get information about the user and program, and project
$user->get_role($program_id, $role, $mentor_organization_id);
$program_data = $cache->get_program_data($program_id);
$user->restrict($program_data !== null);

if ($project_id > 0)
{
    $project_data = $cache->get_project_data($project_id);

    $user->restrict($project_data !== null);
    $user->restrict($project_data['program_id'] == $program_id);
}
else
{
    $project_data = null;
}

// Validate organization_id
if ($organization_id > 0) {
    $sql = "SELECT COUNT(*) AS count " .
           "FROM {$db->prefix}organizations prj " .
           "LEFT JOIN {$db->prefix}programs prg " .
           "ON prg.id = prj.program_id " .
           "WHERE prj.id = :organization_id " .
        "AND prg.id = :program_id ";

    $row = $db->query($sql,
                      array('program_id' => $program_id,
                            'organization_id' => $organization_id),
                      true);
    $user->restrict($row['count'] > 0);
}

$project_permissions = get_project_permissions($program_data, $role, $project_data);
$is_owner = $project_permissions->is_owner;

// Serve the page based on the action
if ($action == 'editor')
{
    $page_title = $project_id == 0 ? $lang->get('submit_proposal') : $lang->get('edit_project');

    if ($project_id == 0)
        $user->restrict($project_permissions->can_submit, true);
    else
        $user->restrict($project_permissions->can_edit, true);

    if ($project_id > 0)
    {
        // Load data from DB only if new data wasn't POSTed
        if (!$project_save)
        {
            // See if the student is passed
            $sql = "SELECT prt.passed FROM {$db->prefix}projects prj " .
                   "  LEFT JOIN {$db->prefix}participants prt " .
                   "ON prj.id = prt.project_id " .
                   "WHERE prj.id = ? " .
                   "AND prt.role = 's'";
            $row = $db->query($sql, $project_id, true);

            $title = $project_data['title'];
            $description = $project_data['description'];
            $organization_id = $project_data['organization_id'];
            $late_edit = $project_data['late_edit'];
        }

        if ($organization_id != $project_data['organization_id'])
        {
            if (!$project_permissions->can_change_organization)
            {
                $organization_change_valid = false;
            }
            else
            {
                // If the organization can be changed, make sure that the target organization
                // is taking new proposals.
                if (!$user->is_admin && $core->timestamp >= $program_data['dl_student'])
                {
                    $organization_data = $cache->get_organization_data($project_data['organization_id']);
                    $organization_change_valid = $organization_data['late_submission'];
                }
                else
                {
                    $organization_change_valid = true;
                }
            }

            if (!$organization_change_valid)
                $organization_id = $project_data['organization_id'];
        }
    }

    // Prefill text from the program admin if no text has been saved yet
    if ($description == '')
      $description = $program_data['application_prefill'];

    // Only admins can mark a project as able to be edited late
    $can_set_late_edit = $user->is_admin;

    // Project was saved
    if ($project_save)
    {
        $user->check_csrf();

        // All fields are mandatory
        if (empty($title) || empty($description) || $organization_id <= 0)
        {
            $error_message = $lang->get('mandatory_all');
        }
        else
        {
            $params = array('program_id' => $program_id,
                            'project_id' => $project_id,
                            'title' => $title,
                            'description' => $description,
                            'organization_id' => $organization_id > 0 ? $organization_id : null,
                            'is_passed' => $is_passed,
                            'new_student' => $new_student,
                            'new_mentor' => $new_mentor,
                            'late_edit' => $late_edit);

            // Are we updating?
            if ($project_id > 0)
            {
                // Update project data
                $sql = "UPDATE {$db->prefix}projects " .
                       "SET title = :title, " .
                       "    description = :description, " .
                       "    organization_id = :organization_id, " .
                       "    late_edit = " . ($can_set_late_edit ? ":late_edit " : "late_edit ") .
                       "WHERE id = :project_id";
                $db->query($sql, $params);

                // Update student name
                if ($user->is_admin && !empty($new_student))
                {
                    // Get existing role of the new student
                    $sql = "SELECT role FROM {$db->prefix}roles " .
                           "WHERE username = :new_student " .
                           "AND program_id = :program_id";
                    $role_data = $db->query($sql, $params, true);

                    // New student has an already defined role
                    if ($role_data !== false)
                    {
                        if ($role_data['role'] != 'm')
                        {
                            // Update role to student
                            $sql = "UPDATE {$db->prefix}roles " .
                                   "SET role = 's' " .
                                   "WHERE username = :new_student " .
                                   "AND program_id = :program_id ";
                            $db->query($sql, $params);
                        }
                        else
                        {
                            $error_message = $lang->get('new_student_mentor');
                        }
                    }
                    else
                    {
                        $sql = "INSERT INTO {$db->prefix}roles " .
                               "(username, program_id, role) " .
                               "VALUES (:new_student, :program_id, 's')";
                        $db->query($sql, $params);
                    }

                    if (empty($error_message))
                    {
                       // Update the project student
                        $sql = "UPDATE {$db->prefix}participants " .
                               "SET username = :new_student, " .
                               "    role = 's' " .
                               "WHERE program_id = :program_id " .
                               "AND project_id = :project_id";
                        $db->query($sql, $params);
                    }
                }

                // Update mentor name
                if ($user->is_admin && !empty($new_mentor))
                {
                    if ($new_mentor == '-')
                        $new_mentors = array();
                    else
                        $new_mentors = preg_split('/\s+/', $new_mentor);

                    foreach ($new_mentors as $mentor) {
                        $params['new_mentor'] = $mentor;

                        // Get existing role of the new mentor
                        $sql = "SELECT role FROM {$db->prefix}roles " .
                               "WHERE username = :new_mentor " .
                               "AND program_id = :program_id";
                        $role_data = $db->query($sql, $params, true);

                        // New mentor has an already defined role
                        if ($role_data !== false)
                        {
                            if ($role_data['role'] != 's')
                            {
                                // Update role to mentor
                                $sql = "UPDATE {$db->prefix}roles " .
                                       "SET role = 'm' " .
                                       "WHERE username = :new_mentor " .
                                       "AND program_id = :program_id ";
                                $db->query($sql, $params);
                            }
                            else
                            {
                                $error_message = $lang->get('new_mentor_student');
                            }
                        }
                        else
                        {
                            $sql = "INSERT INTO {$db->prefix}roles " .
                                   "(username, program_id, role) " .
                                   "VALUES (:new_mentor, :program_id, 'm')";
                            $db->query($sql, $params);
                        }
                    }

                    if (empty($error_message))
                    {
                        // Delete existing mentors of this project
                        $sql = "DELETE FROM {$db->prefix}participants " .
                               "WHERE project_id = :project_id " .
                               "AND role = 'm'";
                        $db->query($sql, $params);

                        foreach ($new_mentors as $mentor) {
                            $params['new_mentor'] = $mentor;

                            // Insert the new mentor
                            $sql = "INSERT INTO {$db->prefix}participants " .
                                   "(username, project_id, program_id, role) " .
                                   "VALUES (:new_mentor, :project_id, :program_id, 'm')";
                            $db->query($sql, $params);
                        }
                    }
                }

                if (empty($error_message))
                {
                    // Purge the project and roles cache
                    $cache->purge(array('projects', 'roles'));

                    // We take the user back to the view project page
                    $core->redirect("?q=view_projects&prg={$program_id}&p={$project_id}");
                }
            }

            else
            {
                // Auto accept project when being created by an admin
                $params['is_accepted'] = $user->is_admin ? 1 : -1;

                // Insert new project
                $sql = "INSERT INTO {$db->prefix}projects " .
                       "(title, description, program_id, organization_id, is_accepted, is_complete, late_edit) " .
                       "VALUES (:title, :description, :program_id, :organization_id, :is_accepted, 0, 0)";
                $db->query($sql, $params);

                // Get the new project ID
                $project_id = $db->get_id();
                $params['project_id'] = $project_id;
                // XXX - this seems wrong when admin is creating
                $params['username'] = $user->username;

                // Insert student data
                $sql = "INSERT INTO {$db->prefix}participants " .
                       "(username, project_id, program_id, role, passed) " .
                       "VALUES (:username, :project_id, :program_id, 's', -1)";
                $db->query($sql, $params);

                $success_message = $lang->get('proposal_submitted');
                $title = '';
                $description = '';
                $organization_id = 0;
                // Message to subscribe to a mailing list - should be a config variable
                // note confusing use of isset() below - = false won't work
                // $show_subscribe = true;

                // Purge the project cache
                $cache->purge('projects');

                // Redirect to the view screen for the project
                $core->redirect("?q=view_projects&prg={$program_id}&p={$project_id}");
            }
        }
    }

    // Determine the cancel URL
    $cancel_url = !empty($return_url) ? "?q=view_projects&amp;prg={$program_id}&amp;p={$project_id}"
                                      : "?q=program_home&amp;prg={$program_id}";

    $escaped_description = htmlspecialchars($description);
    $escaped_description = preg_replace('/\n/', '&#10;', $escaped_description);
    $escaped_description = preg_replace('/\r/', '&#13;', $escaped_description);

    // Assign skin data
    $skin->assign(array(
        'editor_title'          => $page_title,
        'program_id'            => $program_id,
        'project_title'         => htmlspecialchars($title),
        'project_description'   => $escaped_description,
        'new_mentor'            => htmlspecialchars($new_mentor),
        'organization_select'   => build_organization_select($program_id, $organization_id,
                                                             false,
                                                             $project_permissions->late_submission,
                                                             $project_id != 0 && !$project_permissions->can_change_organization),
        'success_message'       => isset($success_message) ? $success_message : '',
        'error_message'         => isset($error_message) ? $error_message : '',
        'success_visibility'    => $skin->visibility(empty($success_message), true),
        'error_visibility'      => $skin->visibility(empty($error_message), true),
        'new_visibility'        => $skin->visibility($project_id == 0),
        'late_submission_visibility' => $skin->visibility($project_id == 0 && $project_permissions->late_submission),
        'late_edit_visibility'  => $skin->visibility($can_set_late_edit),
        'decision_visibility'   => $skin->visibility($project_id > 0 && $can_decide),
        'subscribe_visibility'  => $skin->visibility(isset($show_subscribe)),
        'newuser_visibility'    => $skin->visibility($project_id > 0 && $user->is_admin),
        'late_edit_checked'     => $skin->checked($late_edit == 1),
        'cancel_url'            => $cancel_url,
    ));

    // Output the module
    $module_title = $lang->get('submit_proposal');
    $module_data = $skin->output('tpl_view_projects_editor');
}
else if ($action == 'delete')
{
    $user->restrict($project_permissions->can_delete);

    // Deletion was confirmed
    if ($confirm)
    {
        $user->check_csrf();

        $sql = "DELETE FROM {$db->prefix}participants " .
               "WHERE project_id = ?";
        $db->query($sql, $project_id);

        $sql = "DELETE FROM {$db->prefix}projects " .
               "WHERE id = ?";
        $db->query($sql, $project_id);

        // Purge the project cache
        $cache->purge('projects');

        // Redirect to list page
        $core->redirect("?q=program_home&amp;prg={$program_id}");
    }

    // Assign confirm box data
    $skin->assign(array(
        'message_title'     => $lang->get('confirm_deletion'),
        'message_body'      => $lang->get('confirm_project_del'),
        'cancel_url'        => "?q=view_projects&amp;prg={$program_id}&amp;p={$project_id}",
    ));

    // Output the module
    $module_title = $lang->get('confirm_deletion');
    $module_data = $skin->output('tpl_confirm_box');
}
else if ($action == 'withdraw')
{
    $user->restrict($project_permissions->can_withdraw);

    // Withdraw was confirmed
    if ($confirm)
    {
        $user->check_csrf();

        $sql = "UPDATE {$db->prefix}projects " .
               "SET is_withdrawn = 1 " .
               "WHERE id = ?";
        $db->query($sql, $project_id);

        // Purge the project cache
        $cache->purge('projects');

        // Redirect to project page
        $core->redirect("?q=view_projects&prg={$program_id}&p={$project_id}");
    }

    // Assign confirm box data
    $skin->assign(array(
        'message_title'     => $lang->get('confirm_withdraw'),
        'message_body'      => $lang->get('confirm_withdraw_exp'),
        'cancel_url'        => "?q=view_projects&amp;prg={$program_id}&amp;p={$project_id}",
    ));

    // Output the module
    $module_title = $lang->get('confirm_withdraw');
    $module_data = $skin->output('tpl_confirm_box');
}
else if ($action == 'resubmit')
{
    $user->restrict($project_permissions->can_resubmit);
    $user->check_csrf();

    $sql = "UPDATE {$db->prefix}projects " .
           "SET is_withdrawn = 0 " .
           "WHERE id = ?";
    $db->query($sql, $project_id);

    // Purge the project cache
    $cache->purge('projects');

    // Redirect to project page
    $core->redirect("?q=view_projects&prg={$program_id}&p={$project_id}");
}
else if ($action == 'view')
{
    // Program and Project IDs are mandatory here
    $user->restrict($program_id > 0 && $project_id > 0);
    $user->restrict($project_permissions->can_view);

    if ($project_data['organization_id'] != null)
        $organization_data  = $cache->get_organization_data($project_data['organization_id']);
    else
        $organization_data = null;

    $participant_data = $cache->get("participant_{$project_id}", 'projects');

    if (!$participant_data)
    {
        $sql = "SELECT * FROM {$db->prefix}participants " .
               "WHERE project_id = ?";
        $participant_data = $db->query($sql, $project_id);

        $cache->put("participant_{$project_id}", $participant_data, 'projects');
    }

    // Only allow mentors and admins to view mentorship unless project is accepted
    $can_view_mentor = $project_data['is_accepted'] == 1 || $user->is_admin || $role == 'm';

    // Assign participant data
    $mentors = array();
    $passed = -1;

    foreach ($participant_data as $participant)
    {
        if ($participant['role'] == 's')
        {
            $passed = $participant['passed'];
            $student = $participant['username'];
        }
        else if ($can_view_mentor && $participant['role'] == 'm')
        {
            array_push($mentors, $participant['username']);
        }
    }

    // Convert accepted indicator to yes/no/undecided
    if ($project_data['is_accepted'] == 1)
    {
        $accepted = $lang->get('yes');
    }
    else if ($project_data['is_accepted'] == 0)
    {
        $accepted = $lang->get('no');
    }
    else if ($project_data['is_accepted'] == -1)
    {
        $accepted = $lang->get('undecided');
    }

    // Only mentors and admins can view proposal status before mentor deadline
    if ($role != 'm' && !$user->is_admin && $core->timestamp < $program_data['dl_mentor'])
    {
        $accepted = $lang->get('undecided');
    }

    // Once a mentor's contract is approved or they clicked-through the newer
    // click-through contract, they can no longer remove themselves from mentors
    // without administrative intervention.
    $contract_approved = false;
    if ($role == 'm') {
        $sql = "SELECT contract_approved, contract_accepted_time FROM {$db->prefix}roles r " .
               "WHERE r.program_id = :program_id AND r.username = :username";
        $row = $db->query($sql,
                          array('program_id' => $program_id,
                                'username' => $user->username),
                          true);
        $contract_approved = $row['contract_approved'] != 0 | $row['contract_accepted_time'] != 0;
    }

    $can_remove_mentor = in_array($user->username, $mentors) && !$contract_approved;

    // User removed themselves as mentor
    if ($mentor_remove && $can_remove_mentor)
    {
        $user->check_csrf();

        $sql = "DELETE FROM {$db->prefix}participants " .
               "WHERE username = :username AND project_id = :project_id";
        $db->query($sql, array('username' => $user->username,
                               'project_id' => $project_id));

        $success_message = $lang->get('mentor_removed');
        $index = array_search($user->username, $mentors);
        if ($index !== false)
            unset($mentors[$index]);
        $can_remove_mentor = false;
        $is_owner = false;

        // Purge project data
        $cache->purge('projects');
    }

    // A user can choose to mentor if:
    //  1. He signed up as a mentor for the program, and
    //     is not already a mentor for it.
    //
    // KSoC had:
    //  XXXX 2. Project doesn't already have a mentor
    //       (!$has_mentor)
    //    We allow multiple mentors
    //  XXXX 3. Project hasn't passed mentor deadline
    //       $core->timestamp < $program_data['dl_mentor']
    //    We allow late mentorship
    //  XXXX 4. Project has passed student deadline
    //       $core->timestamp > $program_data['dl_student']
    //    We alloe mentors to sign up early
    $can_mentor = ($role == 'm') && !in_array($user->username, $mentors);

    // User applied as mentor
    if ($mentor_apply && $can_mentor)
    {
        $user->check_csrf();

        $sql = "INSERT INTO {$db->prefix}participants " .
               "(username, project_id, program_id, role) " .
               "VALUES (:username, :project_id, :program_id, 'm')";
        $db->query($sql, array('username' => $user->username,
                               'project_id' => $project_id,
                               'program_id' => $program_id));

        $success_message = $lang->get('mentor_submitted');
        $can_mentor = false;
        $can_remove_mentor = !$contract_approved;
        // Message to subscribe to a mailing list - should be a config variable
        // note confusing use of isset() below - = false won't work
        //$show_subscribe = true;
        array_push($mentors, $user->username);
        $is_owner = $project_data['is_accepted'] == 1;

        // Purge project data
        $cache->purge('projects');
    }

    // Set the return URL (needed when approving the project)
    $return_url = urlencode($core->request_uri());

    // Determine if admin controls are visible or not
    $can_approve = ($project_data['is_accepted'] == -1 || $project_data['is_accepted'] == 0) && $user->is_admin;
    $can_reject  = ($project_data['is_accepted'] == -1 || $project_data['is_accepted'] == 1) && $user->is_admin;

    // Attachments

    $can_view_attachments = $is_owner || $role == 'm' || $user->is_admin;
    $can_delete_attachments = $project_permissions->can_edit;

    if ($can_view_attachments) {
        $sql = "SELECT id, name, description FROM {$db->prefix}attachments " .
               "WHERE project_id = ?";
        $attachment_data = $db->query($sql, $project_id);
    } else {
        $attachment_data = array();
    }

    $attachments_list = '';
    foreach ($attachment_data as $row) {
        $attachment_id = $row['id'];
        $view_url = "?q=attachment&prg={$program_id}&p={$project_id}&i={$attachment_id}";
        $delete_url = "?q=attachment&a=delete&prg={$program_id}&p={$project_id}&i={$attachment_id}";

        $skin->assign(array(
            'attachment_id' => htmlspecialchars($row['id']),
            'name' => htmlspecialchars($row['name']),
            'view_url' => htmlspecialchars($view_url),
            'delete_url' => htmlspecialchars($delete_url),
            'description' => htmlspecialchars($row['description']),
            'delete_visibility' => $skin->visibility($can_delete_attachments),
        ));

        $attachments_list .= $skin->output('tpl_view_project_attachment');
    }

    $has_mentor = count($mentors) > 0;

    if ($has_mentor) {
        $mentor_string = null;
        foreach ($mentors as $mentor) {
            if ($mentor_string != null) {
                $mentor_string .= ", ";
            }
            $mentor_string .= $user->profile($mentor, true);
        }
    } else {
        $mentor_string = "-";
    }

    $mentor_label = count($mentors) > 1 ? $lang->get('mentors') : $lang->get('mentor');

    // Assign final skin data
    $skin->assign(array(
        'program_id'                => $program_id,
        'project_id'                => $project_id,
        'project_title'             => htmlspecialchars($project_data['title']),
        'project_organization'      => htmlspecialchars($organization_data ? $organization_data['title'] : ''),
        'project_description'       => nl2br(htmlspecialchars($project_data['description'])),
        'project_student'           => $user->profile($student, true),
        'mentor_label'              => $mentor_label,
        'project_mentor'            => $mentor_string,
        'project_accepted'          => $accepted,
        'return_url'                => $return_url,
        'attachments_list'          => $attachments_list,
        'success_message'           => isset($success_message) ? $success_message : '',
        'success_visibility'        => $skin->visibility(empty($success_message), true),
        'is_withdrawn_visibility'   => $skin->visibility($project_data['is_withdrawn'] == 1),
        'mentor_visibility'         => $skin->visibility($can_view_mentor),
        'edit_visibility'           => $skin->visibility($project_permissions->can_edit),
        'attach_visibility'         => $skin->visibility($is_owner && $project_permissions->can_edit),
        'attachments_visibility'    => $skin->visibility(count($attachment_data) > 0),
        'delete_visibility'         => $skin->visibility($project_permissions->can_delete),
        'withdraw_visibility'       => $skin->visibility($project_permissions->can_withdraw),
        'resubmit_visibility'       => $skin->visibility($project_permissions->can_resubmit),
        'mentor_project_visibility' => $skin->visibility($can_mentor && !$has_mentor),
        'mentor_project_additional_visibility' => $skin->visibility($can_mentor && $has_mentor),
        'mentor_remove_visibility'  => $skin->visibility($can_remove_mentor),
        'actions_visibility'        => $skin->visibility(($is_owner && $project_permissions->can_edit) ||
                                                         $can_mentor || $can_remove_mentor || $user->is_admin),
        'subscribe_visibility'      => $skin->visibility(isset($show_subscribe)),
        'approve_visibility'        => $skin->visibility($can_approve),
        'reject_visibility'         => $skin->visibility($can_reject),
        'modadm_visibility'         => $skin->visibility($can_approve || $can_reject),
    ));

    // Output the module
    $module_title = $lang->get('view_project');
    $module_data = $skin->output('tpl_view_project');
}
else if ($action == 'user' || $action == 'proposed' || $action == 'accepted' || $action == 'rejected')
{
    // Only admins can see rejected projects
    $user->restrict($action != 'rejected' || ($action == 'rejected' && $user->is_admin));

    // Only admins and mentors can see other people's projects
    $user->restrict($action == 'user' || $role == 'm' || $user->is_admin);;

    // Program ID is mandatory here
    $user->restrict($program_id > 0);

    $params = array('program_id' => $program_id,
                    'username' => $user->username,
                    'i:start' => $limit_start,
                    'i:count' => $config->per_page);

    // Build the queries
    $data_sql = "SELECT p.*, o.title as organization_title FROM {$db->prefix}projects p " .
                "LEFT JOIN {$db->prefix}organizations o on p.organization_id = o.id ";
    $count_sql = "SELECT COUNT(*) AS count FROM {$db->prefix}projects p ";
    $limit = "LIMIT :start, :count";

    // Set action specific page title and query
    // Proposals will continue to appear as 'proposed' even if it is approved
    // for non-admin and non-mentor roles until the mentor deadline
    if ($action == 'user')
    {
        $key = "projects_user_{$program_id}_{$user->username}";

        $title = $lang->get('your_projects');
        $filter = "WHERE p.id IN (SELECT project_id " .
                  "FROM {$db->prefix}participants " .
                  "WHERE username = :username " .
                  "AND program_id = :program_id) ";
    }
    else if ($action == 'proposed')
    {
        $key = "projects_proposed_{$program_id}";

        $is_accepted = 'is_accepted = -1';

        if ($role != 'm' && !$user->is_admin && $core->timestamp < $program_data['dl_mentor'])
        {
            $is_accepted .= ' OR is_accepted = 1';
            $key .= "_nonmoderator";
        }

        $title = $lang->get('proposed_projects');
        $filter = "WHERE {$is_accepted} " .
                  "AND p.program_id = :program_id ";
    }
    else if ($action == 'accepted')
    {
        $key = 'projects_accepted_{$program_id}';

        $is_accepted = 'is_accepted = 1';

        if ($role != 'm' && !$user->is_admin && $core->timestamp < $program_data['dl_mentor'])
        {
            // Just use a random value for is_accepted (2)
            // we don't want to fetch anything here
            $is_accepted = 'is_accepted = 2';
            $key .= "_nonmoderator";
        }

        $title = $lang->get('accepted_projects');
        $filter = "WHERE {$is_accepted} " .
                  "AND p.program_id = :program_id ";
    }
    else if ($action == 'rejected')
    {
        $key = 'projects_rejected_{$program_id}';

        $title = $lang->get('rejected_projects');
        $filter = "WHERE is_accepted = 0 " .
                  "AND p.program_id = :program_id ";
    }

    // Apply filters
    $data_sql  .= $filter;
    $data_sql  .= $limit;
    $count_sql .= $filter;

    // Generate the cache keys
    $data_key  = "data_{$key}_{$limit_start}_{$config->per_page}";
    $count_key = "count_{$key}";

    // Get list data and count
    $list_data  = $cache->get($data_key, 'projects');
    $list_count = $cache->get($count_key, 'projects');

    if (!$list_data)
    {
        $list_data = $db->query($data_sql, $params);
        $cache->put($data_key, $list_data, 'projects');
    }

    if (!$list_count)
    {
        $list_count = $db->query($count_sql, $params, true);
        $cache->put($count_key, $list_count, 'projects');
    }

    // Assign approve/reject flag, we need it everywhere!
    $skin->assign('apprej_visibility', $skin->visibility($action == 'proposed' && $user->is_admin));

    // Set the return URL (needed when approving projects)
    $return_url = urlencode($core->request_uri());

    // Generate the project list
    $key = "skin{$data_key}" . ($user->is_admin ? '1' : '0');
    $projects_list = $cache->get($key, 'projects');

    if (!$projects_list)
    {
        foreach ($list_data as $row)
        {
            $project_title = htmlspecialchars($row['title']);
            $project_desc  = htmlspecialchars($row['description']);

            // Trim the title to 60 characters
            if (strlen($project_title) > 60)
            {
                $project_title = trim(substr($project_title, 0, 60)) . '&hellip;';
            }

            // Trim the description to 150 characters
            if (strlen($project_desc) > 150)
            {
                $project_desc = trim(substr($project_desc, 0, 150)) . '&hellip;';
            }

            // Assign data for each project
            $skin->assign(array(
                'project_title'         => $project_title,
                'project_description'   => nl2br($project_desc),
                'project_organization'  => htmlspecialchars($row['organization_title']),
                'withdrawn_visibility'  => $skin->visibility($row['is_withdrawn'] == 1),
                'project_url'           => "?q=view_projects&amp;prg={$program_id}&amp;p={$row['id']}",
                'approve_url'           => "?q=view_projects&amp;a=approve&amp;prg={$program_id}" .
                                           "&amp;p={$row['id']}&amp;r={$return_url}",
                'reject_url'            => "?q=view_projects&amp;a=reject&amp;prg={$program_id}" .
                                           "&amp;p={$row['id']}&amp;r={$return_url}",
            ));

            $projects_list .= $skin->output('tpl_view_projects_item');
        }

        $cache->put($key, $projects_list, 'projects');
    }

    // Get the pagination
    $pagination = $skin->pagination($list_count['count'], $page);

    // Assign final skin data
    $skin->assign(array(
        'program_id'            => $program_id,
        'view_title'            => $title,
        'projects_list'         => $projects_list,
        'list_pages'            => $pagination,
        'notice_visibility'     => $skin->visibility(count($list_data) == 0),
        'list_visibility'       => $skin->visibility(count($list_data) > 0),
        'pages_visibility'      => $skin->visibility($list_count['count'] > $config->per_page),
    ));

    // Output the module
    $module_title = $title;
    $module_data = $skin->output('tpl_view_projects');
}
else if ($action == 'approve' || $action == 'reject' || $action == 'pending')
{
    $user->check_csrf();

    // This is an admin only action
    $user->restrict(false, true);

    // Program ID, Project ID and return URL are mandatory
    $user->restrict($program_id > 0 && $project_id > 0 && !empty($return_url));

    // Set the appropriate value of the accepted flag
    if ($action == 'approve')
        $flag = 1;
    else if ($action == 'reject')
        $flag = 0;
    else
        $flag = -1;

    // Set the project as approved
    $sql = "UPDATE {$db->prefix}projects " .
           "SET is_accepted = :flag " .
           "WHERE id = :project_id";
    $db->query($sql, array('flag' => $flag,
                           'project_id' => $project_id));

    // Purge the project cache
    $cache->purge('projects');

    // Redirect to return URL
    $core->redirect(urldecode($return_url));
}
else if ($action == 'apply')
{
    $user->restrict($project_permissions->can_apply_student || $project_permissions->can_apply_mentor);

    // We need program ID for this action
    $user->restrict($program_id > 0);

    // Validate category
    $user->restrict(in_array($category, array('student', 'mentor')));

    // When applying as a mentor, an organization must be selected - otherwise
    // we can continue
    if ($category == 'student' || ($organization_id > 0 || $organization_id == -1)) {
        $user->check_csrf();

        if ($category == 'student')
            $organization_id = 0;

        $params = array('program_id' => $program_id,
                        'organization_id' => $organization_id > 0 ? $organization_id : null,
                        'username' => $user->username);

        // Set the new role based on action
        $new_role = $category == 'student' ? 's' : 'i';

        // Allow setting new role based on deadlines
        $user->restrict(($new_role == 's' && $project_permissions->can_apply_student) ||
                        ($new_role == 'i' && $project_permissions->can_apply_mentor));

        // previous mentors can directly sign up to be a mentor for a new program
        if ($new_role == 'i' && $user->isMentorAnyProgram()) {
            $new_role = 'm';
        }

        $params['role'] = $new_role;

        // Insert the new role
        if ($role == 'g') {
            $sql = "INSERT INTO {$db->prefix}roles " .
                       "(username, program_id, organization_id, role) " .
                       "VALUES (:username, :program_id, :organization_id, :role)";
        } else {
            $sql = "UPDATE {$db->prefix}roles " .
                   "SET organization_id = :organization_id, " .
                       "role = :role " .
                   "WHERE username = :username AND program_id = :program_id";
        }

        $db->query($sql, $params);

        // Notify admin with email for new mentor requests
        if ($new_role == 'i')
        {
            $email->assign('mentor_name', $user->username);
            $email->send($config->webmaster, $lang->get('mentor_subject'), 'mentor');
        }

        // Purge the roles cache
        $cache->purge('roles');

        // Redirect to program home
        $core->redirect("?q=program_home&prg={$program_id}");
    }

    $organization_select = build_organization_select($program_id, 0, true, false);

    // Assign final skin data
    $skin->assign(array(
        'program_id'            => $program_id,
        'view_title'            => $title,
        'organization_select'   => $organization_select,
        'submit_url'            => "?q=view_projects&amp;a=apply&ampl;prg={$program_id}",
        'cancel_url'            => "?q=project_home&amp;prg={$program_id}"
    ));

    // Output the module
    $module_title = $lang->get('apply_mentor');
    $module_data = $skin->output('tpl_apply_mentor');
}
else if ($action == 'resign' && ($role == 'm' || $role == 'i'))
{
    // See if mentoring any accepted projected projects

    $params = array('username'   => $user->username,
                    'program_id' => $program_id);

    $sql = "SELECT COUNT(*) as count " .
           "FROM {$db->prefix}projects prj " .
           "LEFT JOIN {$db->prefix}participants prt " .
                "ON prj.id = prt.project_id " .
                "WHERE prt.username = :username " .
                  "AND prt.role = 'm' " .
                  "AND prt.program_id = :program_id " .
                  "AND prj.is_accepted";
    $row = $db->query($sql, $params, true);
    $is_accepted_mentor = $row['count'] > 0;

    if ($is_accepted_mentor) {
        $skin->assign(array(
            'message_title'     => $lang->get('cannot_resign'),
            'message_body'      => $lang->get('cannot_resign_mentor_exp'),
            'return_url'        => "?q=program_home&amp;prg={$program_id}",
        ));

        $module_title = null;
        $module_data = $skin->output('tpl_error_box');
    } else if ($confirm) {
        $user->check_csrf();

        $sql = "DELETE FROM {$db->prefix}participants " .
               "WHERE username = :username " .
                 "AND program_id = :program_id";
        $db->query($sql, $params);

        // For students, we have a 'resigned' role, but it doesn't
        // seem useful for mentors. That means that you can erase
        // your resigned student role by applying as a mentor and
        // resigning, but there's only so much history we can represent
        // in a single character...

        $sql = "DELETE FROM {$db->prefix}roles " .
               "WHERE username = :username " .
                 "AND program_id = :program_id";
        $db->query($sql, $params);

        $core->redirect("?q=program_home&amp;prg={$program_id}");

    } else {
        $skin->assign(array(
            'message_title'     => $lang->get('confirm_resign'),
            'message_body'      => $lang->get('confirm_resign_mentor_exp'),
            'cancel_url'        => "?q=program_home&amp;prg={$program_id}",
        ));

        $module_title = null;
        $module_data = $skin->output('tpl_confirm_box');
    }
}
else if ($action == 'resign')
{
    // Only students can resign (mentors handled above)
    $user->restrict($role == 's');

    // We need program ID for this action
    $user->restrict($program_id > 0);

    $delete_projects = $core->timestamp < $program_data['dl_student'];

    $params = array('program_id' => $program_id,
                    'username' => $user->username,
                    'timestamp' => $core->timestamp);

    if ($confirm)
    {
        $user->check_csrf();

        // If program already started, mark student as failed
        if ($core->timestamp >= $program_data['start_time'])
        {
            $sql = "UPDATE {$db->prefix}participants " .
                   "SET passed = 0 " .
                   "WHERE program_id = :program_id " .
                   "AND username = :username";
            $db->query($sql, $params);
        }

        $sql = "SELECT * FROM {$db->prefix}participants " .
               "WHERE program_id = :program_id " .
               "AND username = :username";
        $project_data = $db->query($sql, $params);

        // Student has one or more proposals
        if ($project_data)
        {
            $projects_ary = array();

            foreach ($project_data as $row)
            {
                $projects_ary[] = $row['project_id'];
            }

            $projects = implode(',', $projects_ary);

            if ($delete_projects) {
                // Delete all the projects
                $sql = "DELETE FROM {$db->prefix}participants " .
                       "WHERE project_id IN ({$projects})";
                $db->query($sql);

                $sql = "DELETE FROM {$db->prefix}projects " .
                       "WHERE id IN ({$projects})";
                $db->query($sql);
            } else {
                $sql = "UPDATE {$db->prefix}projects " .
                       "SET is_withdrawn = 1 " .
                       "WHERE id IN ({$projects})";
                $db->query($sql);
            }
        }

        // Set role as resigned
        $sql = "UPDATE {$db->prefix}roles " .
               "SET role = 'r' " .
               "WHERE program_id = :program_id " .
               "AND username = :username";
        $db->query($sql, $params);

        // Purge the projects and roles cache
        $cache->purge(array('projects', 'roles'));

        // Redirect the user to program home
        $core->redirect("?q=program_home&prg={$program_id}");
    }

    // Assign confirm box data
    $skin->assign(array(
        'message_title'     => $lang->get('confirm_resign'),
        'message_body'      => $delete_projects ?
                                    $lang->get('confirm_resign_exp_delete') :
                                    $lang->get('confirm_resign_exp_withdraw'),
        'cancel_url'        => "?q=program_home&prg={$program_id}",
    ));

    // Output the module
    $module_title = $lang->get('confirm_deletion');
    $module_data = $skin->output('tpl_confirm_box');
}
else
{
    // Unknown action
    $core->redirect($core->path());
}

?>
