<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

$action = $core->variable('a', 'view');
$program_id = 0 + $core->variable('prg', 0);
$project_id = 0 + $core->variable('p', '');
$is_contract = (0 + $core->variable('contract', '')) != 0;
$attachment_id = 0 + $core->variable('i', '');
$username_encoded = $core->variable('u', '');
$return_url = $core->variable('r', '');
$description = $core->variable('description', '', false, true);

$username = urldecode($username_encoded);

$attachment_add = isset($_POST['attachment_add']);
$confirm = isset($_POST['yes']);

// Keeps things simple to require the program ID
$user->restrict($program_id > 0);

if (empty($return_url))
{
    if ($is_contract)
        $return_url ="?q=user_profile&prg={$program_id}";
    else
        $return_url ="?q=view_projects&prg={$program_id}&p={$project_id}";
}

if ($project_id > 0)
    $project_data = $cache->get_project_data($project_id);
else
    $project_data = null;

$user->get_role($program_id, $role, $mentor_organization_id);
$program_data = $cache->get_program_data($program_id);
$project_permissions = get_project_permissions($program_data, $role, $project_data);

$user->restrict($program_data != null);

function validate_ids($program_id, $project_id, $attachment_id, $require_owner)
{
    global $db, $user;

    if ($project_id > 0) {
        $sql = "SELECT COUNT(*) as count " .
               "FROM {$db->prefix}participants prt ";

        if ($attachment_id > 0)
            $sql .= "LEFT JOIN {$db->prefix}attachments a " .
                    "ON a.project_id = prt.project_id ";

        $sql .= "WHERE prt.project_id = :project_id AND " .
                      "prt.program_id = :program_id ";

        if ($attachment_id > 0)
            $sql .= "AND a.id = :attachment_id ";

        if ($require_owner)
            $sql .= "AND prt.username = :username " .
                    "AND prt.role = 's' ";
    } else if ($attachment_id > 0) {
        $sql = "SELECT COUNT(*) as count " .
               "FROM {$db->prefix}attachments a " .
               "WHERE a.program_id = :program_id ";
        if ($require_owner)
            $sql .= "AND a.uploader = :username ";
    } else {
        return true; /* We've already validated $program_id */
    }

    $row = $db->query($sql,
                      array('program_id' => $program_id,
                            'project_id' => $project_id,
                            'attachment_id' => $attachment_id,
                            'username' => $user->username),
                      true);

    return $row['count'] > 0;
}

if ($action == 'add') {
    if ($is_contract)
    {
        $user->restrict($project_id == 0);
    }
    else
    {
        $user->restrict($project_id > 0);
        $user->restrict($project_permissions->can_edit);
    }

    $user->restrict(validate_ids($program_id, $project_id, 0, !$user->is_admin));

    $error_message = '';

    if ($attachment_add) {
        $user->check_csrf();

        if ($error_message === '') {
            if ($description == '') {
                $error_message = $lang->get('upload_description_needed');
            }
        }

        if ($error_message === '') {
            if ($_FILES['file']['error'] == UPLOAD_ERR_NO_FILE)
                $error_message = $lang->get('upload_no_file');
        }

        if ($error_message === '') {
            if ($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE ||
                $_FILES['file']['error'] == UPLOAD_ERR_FORM_SIZE)
                $error_message = $lang->get('upload_too_large');
        }

        if ($error_message === '') {
            if ($_FILES['file']['error'] != UPLOAD_ERR_OK)
                $error_message = $lang->get('upload_failed');
        }

        $size = $_FILES['file']['size'];
        if ($error_message === '') {
            if ($size > 1000000)
                $error_message = $lang->get('upload_too_large');
        }

        $name = basename($_FILES['file']['name']);

        if ($error_message === '') {
            $content_type = '';
            $matches = null;
            if (preg_match('/\.([^.]*)$/', $name, $matches)) {
                $extension = strtolower($matches[1]);
                if ($extension == 'pdf')
                    $content_type = 'application/pdf';
                else if ($extension == 'txt')
                    $content_type = 'text/plain';
                else if ($extension == 'odt')
                    $content_type = 'application/vnd.oasis.opendocument.text';
            }

            if ($content_type === '')
                $error_message = $lang->get('upload_unknown_type');
        }

        if ($error_message === '') {
            $fp = fopen($_FILES['file']['tmp_name'], 'rb');

            $sql = "INSERT INTO {$db->prefix}attachments " .
                   "(project_id, program_id, name, uploader, description, content_type, size, data, is_contract) " .
                   " VALUES (:project_id, :program_id, :name, :uploader, :description, :content_type, :size, :data, :is_contract)";

            $db->query($sql, array('project_id' => $project_id,
                                   'program_id' => $program_id,
                                   'uploader' => $user->username,
                                   'name' => $name,
                                   'description' => $description,
                                   'content_type' => $content_type,
                                   'size' => $size,
                                   'l:data' => $fp,
                                   'is_contract' => $is_contract ? 1 : 0));

            $core->redirect($return_url);
        }
    }

    // Assign skin data
    $skin->assign(array(
        'description'           => htmlspecialchars($description),
        'cancel_url'            => htmlspecialchars($return_url),
        'error_message'         => htmlspecialchars($error_message),
        'error_visibility'      => $skin->visibility($error_message !== '')
    ));

    // Output the module
    if ($is_contract)
    {
        $module_title = $lang->get('upload_contract_document');
        $module_data = $skin->output('tpl_contract_upload');
    }
    else
    {
        $module_title = $lang->get('add_attachment');
        $module_data = $skin->output('tpl_attachment_add');
    }

} else if ($action == 'delete') {
    $user->restrict($attachment_id > 0);
    $user->restrict(validate_ids($program_id, $project_id, $attachment_id, !$user->is_admin));

    if ($project_id > 0)
    {
        $user->restrict($project_permissions->can_edit);
    }
    else
    {
        $sql = "SELECT contract_approved " .
               "FROM {$db->prefix}roles r ".
               "WHERE r.program_id = $program_id " .
               "AND r.username = :username ";
        $row = $db->query($sql,
                          array('program_id' => $program_id,
                                'username' => $user->username),
                          true);
        $user->restrict($row['contract_approved'] == 0);
    }

    // Deletion was confirmed
    if ($confirm)
    {
        $user->check_csrf();

        $sql = "DELETE FROM {$db->prefix}attachments " .
               "WHERE id = ?";
        $db->query($sql, $attachment_id);

        $core->redirect($return_url);
    }

    // Assign confirm box data
    $skin->assign(array(
        'message_title'     => $lang->get('confirm_deletion'),
        'message_body'      => $lang->get('confirm_delete_attachment'),
        'cancel_url'        => htmlspecialchars($return_url)
    ));

    // Output the module
    $module_title = $lang->get('confirm_deletion');
    $module_data = $skin->output('tpl_confirm_box');

} else if ($action == 'view') {
    $user->restrict($attachment_id > 0);

    // Mentors and admins can see all attachments, otherwise require the project owner
    $user->restrict(validate_ids($program_id, $project_id, $attachment_id,
                                 !($user->is_admin || $role == 'm')));

    $sql = "SELECT name, content_type, size, data " .
           "FROM {$db->prefix}attachments where id=?";

    $stmt = $db->dbh->prepare($sql);
    $stmt->execute(array($attachment_id));

    $stmt->bindColumn(1, $name, PDO::PARAM_STR);
    $stmt->bindColumn(2, $content_type, PDO::PARAM_STR);
    $stmt->bindColumn(3, $size, PDO::PARAM_STR);
    $stmt->bindColumn(4, $lob, PDO::PARAM_LOB);
    $stmt->fetch(PDO::FETCH_BOUND);

    $quoted_filename = '"' . preg_replace('/"/', '', $name) . '"';

    header("Content-Type: $content_type");
    header("Content-Disposition: filename=$quoted_filename");
    header("Content-Length: $size");

    // mysql PDO backend seems to load PDO's as a string, not stream
    if (is_string($lob))
        echo $lob;
    else
        fpassthru($lob);

    exit;

} else if ($action == 'approve_contract' || $action == 'unapprove_contract') {
    $user->restrict($username != '');
    $user->restrict($user->is_admin);

    $user->check_csrf();

    $approved = ($action == 'approve_contract') ? 1 : 0;

    $sql = "UPDATE {$db->prefix}roles " .
           "SET contract_approved = :approved " .
           "WHERE username = :username " .
           "AND program_id = :program_id";
    $db->query($sql, array('approved' => $approved,
                           'username' => $username,
                           'program_id' => $program_id));

    $core->redirect($return_url);

} else {
    // Unknown action
    $core->redirect($core->path());
}

?>