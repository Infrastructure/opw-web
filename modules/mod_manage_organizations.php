<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

// Collect some data
$action = $core->variable('a', 'list');
$id = $core->variable('o', 0);
$program_id = 0 + $core->variable('prg', 0);
$title = $core->variable('title', '', false, true);
$late_submission = $core->variable('late_submission', '') == 'on' ? 1 : 0;
$page = $core->variable('pg', 1);
$limit_start = ($page - 1) * $config->per_page;

$organization_save = isset($_POST['organization_save']);
$confirm = isset($_POST['yes']);

$program_data = $cache->get_program_data($program_id);
if ($program_data == null)
    $core->redirect("?q=view_programs");

// Serve the page based on the action
if ($action == 'list')
{
    $organizations_list = '';

    // Get all organizations
    $sql = "SELECT * FROM {$db->prefix}organizations " .
               " WHERE program_id = :program_id " .
           "ORDER BY title " .
           "LIMIT :start, :count";
    $organization_data = $db->query($sql, array('program_id' => $program_id,
                                                'i:start' => $limit_start,
                                                'i:count' => $config->per_page));

    // Get organization count
    $sql = "SELECT COUNT(*) AS count FROM {$db->prefix}organizations";
    $organization_count = $db->query($sql, null, true);

    // Build the list
    foreach ($organization_data as $row)
    {
        // Assign data for this organization
        $skin->assign(array(
            'organization_id'          => $row['id'],
            'organization_title'       => htmlspecialchars($row['title']),
            'late_submission'          => $lang->get($row['late_submission'] ? 'yes' : 'no')
        ));

        $organizations_list .= $skin->output('tpl_manage_organizations_item');
    }

    // Get the pagination
    $pagination = $skin->pagination($organization_count['count'], $page);

    // Assign final skin data
    $skin->assign(array(
        'program_id'         => $program_id,
        'organizations_list' => $organizations_list,
        'list_pages'         => $pagination,
        'notice_visibility'  => $skin->visibility(count($organization_data) == 0),
        'list_visibility'    => $skin->visibility(count($organization_data) > 0),
        'pages_visibility'   => $skin->visibility($organization_count['count'] > $config->per_page),
    ));

    // Output the module
    $module_title = $lang->get('manage_organizations');
    $module_data = $skin->output('tpl_manage_organizations');
}
else if ($action == 'editor')
{
    $page_title = $id == 0 ? $lang->get('add_organization') : $lang->get('edit_organization');

    if ($organization_save)
    {
        $user->check_csrf();

        if (empty($title))
        {
            $error_message = $lang->get('err_mandatory_fields');
        }
        else
        {
            $params = array('id' => $id,
                            'title' => $title,
                            'program_id' => $program_id,
                            'late_submission' => $late_submission);

            if ($id > 0)
            {
                // Update organization data
                $sql = "UPDATE {$db->prefix}organizations " .
                       "SET title = :title, " .
                            "late_submission = :late_submission " .
                       "WHERE id = :id";
                $db->query($sql, $params);
            }
            else
            {
                // Insert organization data
                $sql = "INSERT INTO {$db->prefix}organizations " .
                       "(title, program_id, late_submission) " .
                       "VALUES (:title, :program_id, :late_submission)";
                $db->query($sql, $params);

                // Get the new organization ID
                $params['id'] = $db->get_id();
            }

            // Purge the organizations cache
            $cache->purge('organizations');

            // Redirect to list page
            $core->redirect("?q=manage_organizations&prg={$program_id}");
        }
    }

    // Load data when in edit mode
    if ($id > 0)
    {
        $sql = "SELECT * FROM {$db->prefix}organizations " .
               "WHERE id = ?";
        $row = $db->query($sql, array($id), true);

        // Set loaded data
        $title = $row['title'];
        $late_submission = $row['late_submission'];
    }

    // Assign skin data
    $skin->assign(array(
        'editor_title'      => $page_title,
        'title'             => htmlspecialchars($title),
        'late_submission_checked' => $skin->checked($late_submission == 1),
        'error_message'     => isset($error_message) ? $error_message : '',
        'error_visibility'  => $skin->visibility(isset($error_message)),
        'delete_visibility' => $skin->visibility($id > 0),
        'cancel_url'        => "?q=manage_organizations&amp;prg={$program_id}",
        'delete_url'        => "?q=manage_organizations&amp;a=delete&amp;prg={$program_id}&amp;o={$id}",
    ));

    // Output the module
    $module_title = $page_title;
    $module_data = $skin->output('tpl_manage_organizations_editor');
}
else if ($action == 'delete')
{
    // Deletion was confirmed
    if ($confirm)
    {
        $user->check_csrf();

        $params = array('id' => $id);

//        XXX FIXME
//        $sql = "DELETE FROM {$db->prefix}projects " .
//               "WHERE organization_id = :id";
//       $db->query($sql, $params);

//        XXX FIXME
//       $sql = "DELETE FROM {$db->prefix}coordinators " .
//               "WHERE organization_id = :id";
//        $db->query($sql, $params);


        $sql = "DELETE FROM {$db->prefix}organizations " .
               "WHERE id = :id";
        $db->query($sql, $params);

        // Purge the cache data
        // XXX FIXME
        $cache->purge(array('organizations'));

        // Redirect to list page
        $core->redirect("?q=manage_organizations&prg={$program_id}");
    }

    // Assign confirm box data
    $skin->assign(array(
        'message_title'     => $lang->get('confirm_deletion'),
        'message_body'      => $lang->get('confirm_organization_del'),
        'cancel_url'        => "?q=manage_organizations&amp;a=editor&amp;prg={$program_id}&amp;o={$id}",
    ));

    // Output the module
    $module_title = $lang->get('confirm_deletion');
    $module_data = $skin->output('tpl_confirm_box');
}

?>
