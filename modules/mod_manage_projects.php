<?php
/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

if (!defined('IN_PANDORA')) exit;

// Collect some data
$program_id = 0 + $core->variable('prg', 0);
$organization_id = 0 + $core->variable('o', 0);

$rankings_save = isset($_POST['rankings_save']);

// Validate program and organization_id ID
if ($organization_id > 0)
{
    $sql = "SELECT COUNT(*) AS count " .
           "FROM {$db->prefix}organizations o " .
           "LEFT JOIN {$db->prefix}programs prg " .
           "ON prg.id = o.program_id " .
           "WHERE o.id = :organization_id " .
           "AND prg.id = :program_id ";
}
else
{
    $sql = "SELECT COUNT(*) AS count " .
           "FROM {$db->prefix}programs " .
           "WHERE id = :program_id ";
}

$row = $db->query($sql,
                  array('program_id' => $program_id,
                        'organization_id' => $organization_id),
                  true);

$user->restrict($row['count'] > 0);

$program_data = $cache->get_program_data($program_id);

// Get the role of the user
$user->get_role($program_id, $role, $user_organization_id);

if (!$user->is_admin && $organization_id <= 0)
    $organization_id = $user_organization_id;

$can_edit = $user->is_admin || ($organization_id == $user_organization_id);

// Only admins and mentors can access
$user->restrict($role == 'm', true);

$organization_select = build_organization_select($program_id, $organization_id, false, false,
                                                 false, null, "organizationSelect");

// We want to be able to flag when the user has submitted multiple projects
// and especially when multiple organizations want to accept the same student
// so we query for all projects submitted by users that have submitted
// projects to this organization, and sort out the projects to display afterwards.
if ($organization_id > 0) {
    $sql = "SELECT prj.*, prt.*, pf.fullname FROM {$db->prefix}projects prj " .
               "LEFT JOIN {$db->prefix}participants prt " .
               "ON prj.id = prt.project_id " .
               "LEFT JOIN {$db->prefix}profiles pf " .
               "ON prt.username = pf.username " .
           "WHERE prt.program_id = :program_id AND prt.role = 's' AND prt.username IN (" .
               "SELECT prt.username from {$db->prefix}projects prj " .
                   "LEFT JOIN {$db->prefix}participants prt " .
                   "ON prj.id = prt.project_id " .
               "WHERE prj.organization_id = :organization_id " .
                   "AND prt.role = 's')";
    $raw_data = $db->query($sql, array('program_id' => $program_id,
                                       'organization_id' => $organization_id));
} else {
    $raw_data = array();
}

$list_data = array();
$user_submit_count = array();
$user_accept_count = array();

foreach ($raw_data as $row) {
    if ($row['organization_id'] == $organization_id)
        $list_data[] = $row;

    $username = $row['username'];
    if (!isset($user_submit_count[$username]))
        $user_submit_count[$username] = 0;
    if (!isset($user_accept_count[$username]))
        $user_accept_count[$username] = 0;

    if ($row['is_withdrawn'] != 1) {
        $user_submit_count[$username]++;
        if (opinion_is_accept ($row['org_opinion']))
            $user_accept_count[$username]++;
    }
}

// See if we need to save anything
if ($rankings_save && $can_edit) {
    $user->check_csrf();

    foreach ($list_data as &$row) {
        $project_id = $row['project_id'];
        $ranking_field_name = 'ranking' . $project_id;

        if (isset($_POST[$ranking_field_name])) {
            $value = trim($_POST[$ranking_field_name]);
            if ($value === '')
                $new_ranking = -1;
            else
                $new_ranking = (float)$value;

            if ($new_ranking < 0)
                $new_ranking = -1;

            if (abs($new_ranking - $row['ranking']) > 0.001) {
                $sql = "UPDATE {$db->prefix}projects " .
                       "SET ranking = :ranking " .
                       "WHERE id = :project_id";
                $db->query($sql, array('ranking' => $new_ranking,
                                       'project_id' => $project_id));
                $row['ranking'] = $new_ranking;
            }
        }

        $opinion_field_name = 'opinion' . $project_id;
        if (isset($_POST[$opinion_field_name])) {
            $value = $_POST[$opinion_field_name];
            if (opinion_is_valid($value)) {
                $sql = "UPDATE {$db->prefix}projects " .
                       "SET org_opinion = :org_opinion " .
                       "WHERE id = :project_id";
                $db->query($sql, array('org_opinion' => $value,
                                       'project_id' => $project_id));

                $old_accept_count = opinion_is_accept($row['org_opinion']) ? 1 : 0;
                $new_accept_count = opinion_is_accept($value) ? 1 : 0;
                $user_accept_count[$row['username']] += ($new_accept_count - $old_accept_count);

                $row['org_opinion'] = $value;
            }
        }
    }
}

// Now order the rows - we do this client side for two reasons:
// because we change values when saving, and so that we can special-case
// negative values

function compare_rankings($a, $b) {
    $arank = $a['ranking'];
    $brank = $b['ranking'];

    if ($arank < 0)
        return ($brank < 0) ? 0 : 1;

    if ($brank < 0)
        return ($arank < 0) ? 0 : -1;

    if ($arank < $brank)
        return -1;
    else
        return ($arank == $brank) ? 0 : 1;
}

usort($list_data, "compare_rankings");

$skin->assign(array('apprej_visibility' => $skin->visibility($user->is_admin)));

// Set the return URL (needed when approving projects)
$return_url = urlencode($core->request_uri());

$projects_list = '';

$a = 1;

foreach ($list_data as &$row)
{
    $project_title = htmlspecialchars($row['title']);
    $username = $row['username'];
    $submit_count = $user_submit_count[$username];
    $accept_count = $user_accept_count[$username];
    $profile = $user->profile($username, true, $row['fullname']);

    // Trim the title to 60 characters
    if (strlen($project_title) > 60)
    {
        $project_title = trim(substr($project_title, 0, 60)) . '&hellip;';
    }

    if ($row['ranking'] < 0)
        $ranking = '';
    else
        $ranking = number_format($row['ranking'], 1);

    $project_id = $row['project_id'];

    // Assign data for each project
    $skin->assign(array(
        'project_title'         => $project_title,
        'project_applicant'     => $profile,
        'ranking'               => $ranking,
        'ranking_name'          => 'ranking' . $project_id,
        'badges'                => build_project_badges($role, $row, $submit_count, $accept_count),
        'disabled'              => $can_edit ? '' : ' disabled',
        'opinion_select'        => build_opinion_select($program_data, $row['org_opinion'], 'opinion' . $project_id, !$can_edit),
        'project_url'           => "?q=view_projects&amp;prg={$program_id}&amp;p={$project_id}",
        'approve_url'           => "?q=view_projects&amp;a=approve&amp;prg={$program_id}" .
                                   "&amp;p={$project_id}&amp;r={$return_url}",
        'pending_url'           => "?q=view_projects&amp;a=pending&amp;prg={$program_id}" .
                                   "&amp;p={$project_id}&amp;r={$return_url}",
        'reject_url'            => "?q=view_projects&amp;a=reject&amp;prg={$program_id}" .
                                   "&amp;p={$project_id}&amp;r={$return_url}",
    ));

    $projects_list .= $skin->output('tpl_manage_projects_item');
}

$title = $lang->get('manage_projects');

// Assign final skin data
$skin->assign(array(
    'program_id'            => $program_id,
    'view_title'            => $title,
    'organization_select'   => $organization_select,
    'projects_list'         => $projects_list,
    'disabled'              => $can_edit ? '' : ' disabled',
    'admin_visibility'      => $skin->visibility($user->is_admin),
    'notice_visibility'     => $skin->visibility($organization_id > 0 && count($list_data) == 0),
    'list_visibility'       => $skin->visibility(count($list_data) > 0)
));

// Output the module
$module_title = $title;
$module_data = $skin->output('tpl_manage_projects');

?>
