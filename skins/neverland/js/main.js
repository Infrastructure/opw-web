/**
* Pandora v1
* @license GPLv3 - http://www.opensource.org/licenses/GPL-3.0
* @copyright (c) 2012 KDE. All rights reserved.
*/

function submitForm(evt, url) {
    evt.preventDefault();
    var form = document.getElementsByTagName("form")[0];
    var oldAction = form.action;
    form.action = url;
    try {
        form.submit();
    } catch(e) {
        form.action = oldAction;
        thow(e);
    }
}

// Startup function
$(document).ready(function() {
    $("#block-link").removeClass("hidden");
    $("#block-deadlines").hide();

    $("#block-link").click(function() {
        $(this).hide();

        $("#block-deadlines")
        .css({
            display: "inline-block",
            height: 0,
            opacity: 0
        })
        .animate({
            opacity: 1,
            height: 47,
        });

        return false;
    });

    if (typeof($().datetimepicker) == "function") {
        $(".datepicker").datetimepicker({
            dateFormat: "M dd yy,",
            timeFormat: "hh:mm tt",
            ampm: true,
            changeMonth: true,
            changeYear: true,
        });
    }
});